var canviz;

Event.observe(window, 'load', function() {
	loadCanviz();
});

function doLoadCanviz(base64XDot) {
	canviz = null;
	canviz = new Canviz('canviz_container');
	canviz.setScale(1);
	canviz.parse(window.atob(base64XDot));
	canviz.draw();
}

function set_graph_scale(inc) {
	canviz.setScale(inc);
	canviz.draw();
}
