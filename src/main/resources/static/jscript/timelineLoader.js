Event.observe(window, 'load', function() {
    loadTimeline();

});

Event.observe(window, 'resize', function() {
    onResizeTimeline();
});

var timeLine;
var Timeline_urlPrefix = "https://api.simile-widgets.org/timeline/2.3.1/";

var resizeTimerID = null;
function onResizeTimeline() {
    if (resizeTimerID == null) {
        resizeTimerID = window.setTimeout(function() {
            resizeTimerID = null;
            timeLine.layout();
        }, 500);
    }
}

function getBandInfo(timeLineEventSource, theme, width, intervalUnit, intervalPixels, layout, startDate, endDate, multipleNormal, multipleHot) {
    var startDateObj = Timeline.DateTime.parseIso8601DateTime(startDate);
    var endDateObj = Timeline.DateTime.parseIso8601DateTime(endDate);

  var msPerHour = 1000 * 60 * 60;
  var msPerDay = msPerHour * 24;
  var startDay = Math.floor(startDateObj.getTime() / msPerDay);
  var endDay = Math.ceil(endDateObj.getTime() / msPerDay);
  var zones = new Array();
  for (day = startDay ; day <= endDay; day++) {
    var startNight = day * msPerDay - 4 * msPerHour + 1000;
    var endNight = day * msPerDay + 8 * msPerHour + 1000;
    var zoneNight = { 
      start: new Date(startNight),
      end: new Date(endNight),
      magnify: 0.2,
      multiple: multipleHot,
      unit: intervalUnit,
    };
    zones.push(zoneNight);

    var zoneDay = { 
      start: new Date(endNight),
      end: new Date(startNight + 24 * msPerHour),
      magnify: 1.0,
      multiple: multipleNormal,
      unit: intervalUnit,
    };
    zones.push(zoneDay);
  }
  return Timeline.createHotZoneBandInfo({
        zones: zones,
        eventSource : timeLineEventSource,
        showEventText : false,
        trackHeight : 0.5,
        trackGap : 0.2,
        width : width,
        intervalUnit : intervalUnit,
        intervalPixels : intervalPixels,
        date : startDate,
        theme: theme,
        layout: layout
    });
}

function doLoadTimeLine(startDate, endDate, xmlContent64) {
    var timeLineEventSource = new Timeline.DefaultEventSource();

            var theme1 = Timeline.ClassicTheme.create();
            theme1.autoWidth = true;

  var msPerHour = 1000 * 60 * 60;
  var msPerDay = msPerHour * 24;

    var startDateObj = Timeline.DateTime.parseIso8601DateTime(startDate);
    var endDateObj = Timeline.DateTime.parseIso8601DateTime(endDate);

            theme1.timeline_start = new Date(startDateObj.getTime() - msPerDay * 1);
            theme1.timeline_stop  = new Date(endDateObj.getTime() + msPerDay * 1);
            
    var bandInfos = [ 
      getBandInfo(timeLineEventSource, theme1, "4%", Timeline.DateTime.DAY, 150, 'overview', startDate, endDate, 1, 1),
      getBandInfo(timeLineEventSource, theme1, "32%", Timeline.DateTime.HOUR, 25, 'original', startDate, endDate, 2, 4),
      getBandInfo(timeLineEventSource, theme1, "32%", Timeline.DateTime.HOUR, 100, 'original', startDate, endDate, 1, 2),
      getBandInfo(timeLineEventSource, theme1, "32%", Timeline.DateTime.MINUTE, 40, 'original', startDate, endDate, 1, 2)
    ];

    bandInfos[2].syncWith = 3;
    bandInfos[2].highlight = true;
    bandInfos[1].syncWith = 2;
    bandInfos[1].highlight = true;
    bandInfos[0].syncWith = 1;
    bandInfos[0].highlight = true;

    resetTimeLine();
    
    xmlContent = window.atob(xmlContent64);

    timeLine = null;
    timeLine = Timeline.create(document.getElementById("tl"),
            bandInfos);

    var xmlDoc = null;
    if (window.DOMParser) {
        parser = new DOMParser();
        xmlDoc = parser.parseFromString(xmlContent, "text/xml");
    } else // Internet Explorer
    {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(xmlContent);
    }
    timeLineEventSource.loadXML(xmlDoc, "nope.xml");

    timeLine.layout();
    timeLine.getBand(3).setCenterVisibleDate(
            Timeline.DateTime.parseIso8601DateTime(startDate));
}


function resetTimeLine() {

Timeline.OriginalEventPainter.prototype._paintEventIcon = function(evt, iconTrack, left, metrics, theme, tapeHeight) {
    // If no tape, then paint the icon in the middle of the track.
    // If there is a tape, paint the icon below the tape + impreciseIconMargin
    var icon = evt.getIcon();
    icon = icon != null ? icon : metrics.icon;

    var top; // top of the icon
    var middle = metrics.trackOffset + iconTrack * metrics.trackIncrement + tapeHeight / 2;
    top = Math.round(middle - metrics.iconHeight / 2);

    var img = SimileAjax.Graphics.createTranslucentImage(icon);
    var iconDiv = this._timeline.getDocument().createElement("div");
    iconDiv.className = this._getElClassName('timeline-event-icon', evt, 'icon');
    iconDiv.id = this._encodeEventElID('icon', evt);
    iconDiv.style.left = left + "px";
    iconDiv.style.top = top + "px";
    iconDiv.style.zIndex = 116;
    iconDiv.appendChild(img);

    if(evt._title != null)
        iconDiv.title = evt._title;

    this._eventLayer.appendChild(iconDiv);

    return {
        left:   left,
        top:    top,
        width:  metrics.iconWidth,
        height: metrics.iconHeight,
        elmt:   iconDiv
    };
};

Timeline.GregorianDateLabeller.prototype.defaultLabelInterval = function(date, intervalUnit) {
    var text;
    var emphasized = false;

    date = SimileAjax.DateTime.removeTimeZoneOffset(date, this._timeZone);

    switch(intervalUnit) {
    case SimileAjax.DateTime.MILLISECOND:
        text = date.getUTCMilliseconds();
        break;
    case SimileAjax.DateTime.SECOND:
        text = date.getUTCSeconds();
        break;
    case SimileAjax.DateTime.MINUTE:
        var m = date.getUTCMinutes();
        if (m == 0) {
            text = date.getUTCHours() + ":00";
            emphasized = true;
        } else {
            text = m;
        }
        break;
    case SimileAjax.DateTime.HOUR:
        var h = date.getUTCHours();
        if (h == 0) {
            text = Timeline.GregorianDateLabeller.getMonthName(date.getUTCMonth(), this._locale) + " " + date.getUTCDate();
            emphasized = true;
        } else {
          text = date.getUTCHours() + "hr";
        }
        break;
    case SimileAjax.DateTime.DAY:
        text = Timeline.GregorianDateLabeller.getMonthName(date.getUTCMonth(), this._locale) + " " + date.getUTCDate();
        break;
    case SimileAjax.DateTime.WEEK:
        text = Timeline.GregorianDateLabeller.getMonthName(date.getUTCMonth(), this._locale) + " " + date.getUTCDate();
        break;
    case SimileAjax.DateTime.MONTH:
        var m = date.getUTCMonth();
        if (m != 0) {
            text = Timeline.GregorianDateLabeller.getMonthName(m, this._locale);
            break;
        } // else, fall through
    case SimileAjax.DateTime.YEAR:
    case SimileAjax.DateTime.DECADE:
    case SimileAjax.DateTime.CENTURY:
    case SimileAjax.DateTime.MILLENNIUM:
        var y = date.getUTCFullYear();
        if (y > 0) {
            text = date.getUTCFullYear();
        } else {
            text = (1 - y) + "BC";
        }
        emphasized =
            (intervalUnit == SimileAjax.DateTime.MONTH) ||
            (intervalUnit == SimileAjax.DateTime.DECADE && y % 100 == 0) ||
            (intervalUnit == SimileAjax.DateTime.CENTURY && y % 1000 == 0);
        break;
    default:
        text = date.toUTCString();
    }
    return { text: text, emphasized: emphasized };
}


}
