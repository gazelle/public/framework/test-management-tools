window.onload = function () {
    var currentIp = document.getElementById("keywordhidden").value;
    var iheConcepts = Seam.Component.getInstance("IHEConceptsRemoteService");
    iheConcepts.getStringOfGraphForGivenListOfTransactionLinks(currentIp, draw);
};

function draw(str) {
    var image = Viz(str);
    jq162('#graph_container').append(image);

    var panZoom = svgPanZoom('#graph_container svg');

    document.getElementById('zoom-in').addEventListener('click', function (ev) {
        ev.preventDefault();

        panZoom.zoomIn()
    });

    document.getElementById('zoom-out').addEventListener('click', function (ev) {
        ev.preventDefault();

        panZoom.zoomOut();
    });
}