var timer;
var graph;

var FRAME_WIDTH;
var FRAME_HEIGHT;
var FRAME_LEFT;
var FRAME_TOP;

var iheConcepts;

var thisIp;
var thisDomain;
var thisActor;
var thisTransaction;

var currentObject;
var currentPage = -1;

var origin;
var zTop = 11;
var angle = 90;

var context;

var tableDescriptions = new Array();
var objectIndex = -1; 					// index in the table, -1 if object isn't in the table

var numIpMaxPerPage = 25;
var min = 0, max = 0;
var currentIpList;

var IE = document.all ? true : false;

function loadOverview() {

    if (IE) {
        FRAME_WIDTH = parseInt(document.body.offsetWidth) - 100;
        FRAME_HEIGHT = (parseInt(document.getElementById("displayOverview").style.height));
    } else {
        FRAME_WIDTH = parseInt(window.innerWidth) - 100;
        FRAME_HEIGHT = (parseInt(document.getElementById("displayOverview").style.height));
    }

    FRAME_LEFT = parseInt(document.getElementById('frame').style.left);
    FRAME_TOP = parseInt(document.getElementById('frame').style.top);

    //FRAME_WIDTH -= (parseInt(document.getElementById('frame').style.left)*2);
    //FRAME_HEIGHT -= (parseInt(document.getElementById('frame').style.top)+20);

    doStagger = true;

//	--------- Callback lists --------- //

    function domainKeywordsCallback(domainKeyword) {
        timer.start();

        if (domainKeyword.length > 10) {
            angle = 50;
        }
        if (domainKeyword.length > 30) {
            angle = 20;
        }

        for (var i = 0; i < domainKeyword.length; i++) {

            var domNode = origin.addChild(new TreeNode("dom_" + domainKeyword[i], 110, angle, parseInt(FRAME_WIDTH / 2), parseInt(FRAME_HEIGHT / 2)));

            domNode['fan_angle'] += 4 * domNode['children'].length;
            domNode['domNode'].innerHTML = "<a href='#' onclick='loadDom(event);' onmouseover='javascript:domainOnMouseOver(event);' onmouseout='javascript:onMouseOut();' >" + domainKeyword[i] + "</a>";
            domNode.updateChildren();
        }
    }

    function ipKeywordsCallback(ipList) {

        timer.start();

        if (ipList.length > 10) {
            angle = 50;
        }
        if (ipList.length > 30) {
            angle = 20;
        }

        if (ipList.length > numIpMaxPerPage) {

            var nbPage = Math.floor(ipList.length / numIpMaxPerPage);

            for (var j = 0; j <= nbPage; j++) {
                document.getElementById("ds_table").rows[0].insertCell(3 + j);
                var cellAdded = document.getElementById("ds_table").rows[0].cells[3 + j];
                cellAdded.innerHTML = j + 1;

                if (IE) {
                    cellAdded.attachEvent('onclick', goSpecificPageIP);
                } else {
                    cellAdded.addEventListener("click", goSpecificPageIP, false);
                }
            }

            currentPage = 1;
            min = 0;
            max = numIpMaxPerPage;
            currentIpList = ipList;
            displayIp(currentIpList);

        } else {
            for (var i = 0; i < ipList.length; i++) {
                var ipNode = origin.addChild(new TreeNode("ip_" + ipList[i], 110, angle, parseInt(FRAME_WIDTH / 2), parseInt(FRAME_HEIGHT / 2)));

                ipNode['fan_angle'] += 4 * ipNode['children'].length;
                ipNode['domNode'].innerHTML = "<a href='#' onclick='loadIP(event);' onmouseover='javascript:ipOnMouseOver(event);' onmouseout='javascript:onMouseOut();' >" + ipList[i] + "</a>";
                ipNode.updateChildren();
                document.getElementById("frame").appendChild(ipNode['domNode']);
            }

        }
    }

    function actorKeywordsCallback(actorList) {
        timer.start();

        if (actorList.length > 10) {
            angle = 50;
        }
        if (actorList.length > 30) {
            angle = 20;
        }

        for (var i = 0; i < actorList.length; i++) {

            var actorNode = origin.addChild(new TreeNode("actor_" + actorList[i], 110, angle, parseInt(FRAME_WIDTH / 2), parseInt(FRAME_HEIGHT / 2)));

            actorNode['fan_angle'] += 4 * actorNode['children'].length;
            actorNode['domNode'].innerHTML = "<a href='#' onclick='loadActor(event);' onmouseover='javascript:actorOnMouseOver(event);' onmouseout='javascript:onMouseOut();' >" + actorList[i] + "</a>";
            actorNode.updateChildren();
            document.getElementById("frame").appendChild(actorNode['domNode']);
        }
    }

    function transactionKeywordsCallback(transList) {
        if (transList.length > 0) {
            if (transList.length == 1 && transList[0] == "NULL") {
            } else {
                timer.start();

                var firstNode = origin.addChild(new TreeNode("trans_bubble", 110, angle, parseInt(FRAME_WIDTH / 2), parseInt(FRAME_HEIGHT / 2)));
                firstNode['domNode'].innerHTML = "<img src='../jscript/bubbleblue.gif' onmouseover='javascript:bubbleOnMouseOver(event);' onmouseout='javascript:onMouseOut();' alt='Transactions' />";
                document.getElementById("frame").appendChild(firstNode['domNode']);

                for (var i = 0; i < transList.length; i++) {
                    if (transList[i] != "NULL") {
                        if (transList.length > 10) {
                            angle = 50;
                        }
                        if (transList.length > 30) {
                            angle = 20;
                        }
                        var transNode = firstNode.addChild(new TreeNode("trans_" + transList[i], 0, 0, firstNode['position']['x'], firstNode['position']['y']));

                        firstNode['fan_angle'] += 4 * firstNode['children'].length;
                        transNode['domNode'].innerHTML = "<a href='#' onclick='loadTrans(event);' onmouseover='javascript:transactionOnMouseOver(event);' onmouseout='javascript:onMouseOut();' >" + transList[i] + "</a>";
                        firstNode.updateChildren();
                        document.getElementById("frame").appendChild(transNode['domNode']);
                    }
                }
            }
        }
    }

    function ipoKeywordsCallback(ipoList) {
        if (ipoList.length > 0) {
            if (ipoList.length == 1 && ipoList[0] == "NONE") {
            } else {
                timer.start();

                var firstNode = origin.addChild(new TreeNode("ipo_bubble", 110, angle, parseInt(FRAME_WIDTH / 2), parseInt(FRAME_HEIGHT / 2)));
                firstNode['domNode'].innerHTML = "<img src='../jscript/bubblepink.gif' onmouseover='javascript:bubbleOnMouseOver(event);' onmouseout='javascript:onMouseOut();' alt='Integration profiles option' />";
                document.getElementById("frame").appendChild(firstNode['domNode']);

                for (var i = 0; i < ipoList.length; i++) {
                    if (ipoList[i] != "NONE") {
                        if (ipoList.length > 10) {
                            angle = 50;
                        }
                        if (ipoList.length > 30) {
                            angle = 20;
                        }
                        var ipoNode = firstNode.addChild(new TreeNode("ipo_" + ipoList[i], 0, 0, firstNode['position']['x'], firstNode['position']['y']));

                        firstNode['fan_angle'] += 4 * firstNode['children'].length;
                        ipoNode['domNode'].innerHTML = "<a onmouseover='javascript:ipoOnMouseOver(event);' onmouseout='javascript:onMouseOut();' >" + ipoList[i] + "</a>";
                        firstNode.updateChildren();
                        document.getElementById("frame").appendChild(ipoNode['domNode']);
                    }
                }
            }
        }
    }

    function initiatorKeywordsCallback(initList) {
        if (initList.length > 0) {
            timer.start();

            var firstNode = origin.addChild(new TreeNode("init_bubble", 110, angle, parseInt(FRAME_WIDTH / 2), parseInt(FRAME_HEIGHT / 2)));
            firstNode['domNode'].innerHTML = "<img src='../jscript/bubbleyellow.gif' onmouseover='javascript:bubbleOnMouseOver(event);' onmouseout='javascript:onMouseOut();' alt='Initiators' />";
            document.getElementById("frame").appendChild(firstNode['domNode']);

            for (var i = 0; i < initList.length; i++) {
                if (initList.length > 10) {
                    angle = 50;
                }
                if (initList.length > 30) {
                    angle = 20;
                }
                var initNode = firstNode.addChild(new TreeNode("init_" + initList[i], 0, 0, firstNode['position']['x'], firstNode['position']['y']));

                firstNode['fan_angle'] += 4 * firstNode['children'].length;
                initNode['domNode'].innerHTML = "<a onmouseover='javascript:actorOnMouseOver(event);' onmouseout='javascript:onMouseOut();' >" + initList[i] + "</a>";
                firstNode.updateChildren();
                document.getElementById("frame").appendChild(initNode['domNode']);
            }
        }
    }

    function responderKeywordsCallback(respondList) {
        if (respondList.length > 0) {
            timer.start();

            var firstNode = origin.addChild(new TreeNode("respond_bubble", 110, angle, parseInt(FRAME_WIDTH / 2), parseInt(FRAME_HEIGHT / 2)));
            firstNode['domNode'].innerHTML = "<img src='../jscript/bubblegreen.gif' onmouseover='javascript:bubbleOnMouseOver(event);' onmouseout='javascript:onMouseOut();' alt='Responders' />";
            document.getElementById("frame").appendChild(firstNode['domNode']);

            for (var i = 0; i < respondList.length; i++) {
                if (respondList.length > 10) {
                    angle = 50;
                }
                if (respondList.length > 30) {
                    angle = 20;
                }
                var respondNode = firstNode.addChild(new TreeNode("respond_" + respondList[i], 0, 0, firstNode['position']['x'], firstNode['position']['y']));

                firstNode['fan_angle'] += 4 * firstNode['children'].length;
                respondNode['domNode'].innerHTML = "<a onmouseover='javascript:actorOnMouseOver(event);' onmouseout='javascript:onMouseOut();' >" + respondList[i] + "</a>";
                firstNode.updateChildren();
                document.getElementById("frame").appendChild(respondNode['domNode']);
            }
        }
    }

//	create a timer control
    timer = new TimerControl();
    timer.initialize(1);

//	instantiate the graph
    graph = new TreeGraph(FRAME_WIDTH, FRAME_HEIGHT, FRAME_TOP, FRAME_LEFT);

//	subscribe the graph to the control timer
    timer.subscribe(graph);


    document.getElementById("datascroller").style.visibility = "hidden";

//	Seam.Remoting.setDebug(true);
    iheConcepts = Seam.Component.getInstance("IHEConceptsRemoteService");

    if (document.location.href.indexOf('trans=') != -1) {
        var url = document.location.href;
        var domainEncode = url.substring(url.indexOf('dom=') + 4, url.indexOf('ip=') - 1);
        thisDomain = decodeURI(domainEncode);
        var ipEncode = url.substring(url.indexOf('ip=') + 3, url.indexOf('actor=') - 1);
        thisIp = decodeURI(ipEncode);
        var actorEncode = url.substring(url.indexOf('actor=') + 6, url.indexOf('trans=') - 1);
        thisActor = decodeURI(actorEncode);
        var transEncode = url.substring(url.indexOf('trans=') + 6);
        thisTransaction = decodeURI(transEncode);

        iheConcepts.getListOfRespondersForGivenTransaction(thisTransaction, responderKeywordsCallback);
        iheConcepts.getListOfInitiatorsForGivenTransaction(thisTransaction, initiatorKeywordsCallback);

        url = url.split("?");

        createOrigin("<a href='" + url[0] + "?" + url[1] + "?" + url[2] + "?" + url[3] + "'>" + thisTransaction + "</a>");
        document.getElementById("breadcrumb").innerHTML = "<a href='" + url[0] + "'> TF </a> > Domain : <a href='" + url[0] + "?" + url[1] + "'>" + thisDomain + "</a>" + " > Integration profile : <a href='" + url[0] + "?" + url[1] + "?" + url[2] + "'>" + thisIp + "</a> > Actor : <a href='" + url[0] + "?" + url[1] + "?" + url[2] + "?" + url[3] + "'>" + thisActor + "</a> > Transaction : <a href='" + document.location.href + "'>" + thisTransaction + "</a>";


    } else if ((document.location.href.indexOf('ip=') != -1) && (document.location.href.indexOf('actor=') != -1)) {
        var url = document.location.href;
        var domainEncode = url.substring(url.indexOf('dom=') + 4, url.indexOf('ip=') - 1);
        thisDomain = decodeURI(domainEncode);
        var ipEncode = url.substring(url.indexOf('ip=') + 3, url.indexOf('actor=') - 1);
        thisIp = decodeURI(ipEncode);
        var actorEncode = url.substring(url.indexOf('actor=') + 6);
        thisActor = decodeURI(actorEncode);

        iheConcepts.getListOfTransactionsForGivenActorAndIP(thisIp, thisActor, transactionKeywordsCallback);
        iheConcepts.getListOfIPOForGivenActorAndIP(thisIp, thisActor, ipoKeywordsCallback);

        url = url.split("?");

        createOrigin("<a href='" + url[0] + "?" + url[1] + "?" + url[2] + "'>" + thisActor + "</a>");
        document.getElementById("breadcrumb").innerHTML = "<a href='" + url[0] + "'> TF </a> > Domain : <a href='" + url[0] + "?" + url[1] + "'>" + thisDomain + "</a>" + " > Integration profile : <a href='" + url[0] + "?" + url[1] + "?" + url[2] + "'>" + thisIp + "</a> > Actor : <a href='" + document.location.href + "'>" + thisActor + "</a>";


    } else if ((document.location.href.indexOf('ip=') != -1) && (document.location.href.indexOf('actor=') == -1)) {
        var url = document.location.href;
        var domainEncode = url.substring(url.indexOf('dom=') + 4, url.indexOf('ip=') - 1);
        thisDomain = decodeURI(domainEncode);
        var ipEncode = url.substring(url.indexOf('ip=') + 3);
        thisIp = decodeURI(ipEncode);

        iheConcepts.getListOfActorsForGivenIntegrationProfile(thisIp, actorKeywordsCallback);

        url = url.split("?");

        createOrigin("<a href='" + url[0] + "?" + url[1] + "'>" + thisIp + "</a>");
        document.getElementById("breadcrumb").innerHTML = "<a href='" + url[0] + "'> TF </a> > Domain : <a href='" + url[0] + "?" + url[1] + "'>" + thisDomain + "</a>" + " > Integration profile : <a href='" + document.location.href + "'>" + thisIp + "</a>";


    } else if (document.location.href.indexOf('dom=') != -1) {
        var domainEncode = document.location.href.substring(document.location.href.indexOf('=') + 1);
        thisDomain = decodeURI(domainEncode);
        iheConcepts.getListOfIntegrationProfilesForGivenDomain(thisDomain, ipKeywordsCallback);

        var url = document.location.href;
        url = url.split("?");

        createOrigin("<a href='" + url[0] + "'>" + thisDomain + "</a>");
        document.getElementById("breadcrumb").innerHTML = "<a href='" + url[0] + "'> TF </a> > Domain : <a href='" + document.location.href + "'>" + thisDomain + "</a>";


    } else {
        iheConcepts.getListOfPossibleDomains(domainKeywordsCallback);

        var url = document.location.href;
        url = url.split("?");
        createOrigin("<a>TF</a>");
        document.getElementById("breadcrumb").innerHTML = "<a>TF</a>";
    }

    var t = setTimeout("timer.stop()", 10000);
}

// ------- Callback object ------- //

function objectCallback(object) {
    if (objectIndex == -1) {
        tableDescriptions[tableDescriptions.length] = object;
    }

    var url = document.location.href;
    url = url.split("tf");

    if (currentObject != undefined) {
        var definition = '<div class="descriptiontitle"><table style="width:100%"><tr><td><a href="' + url[0] + currentObject + '.seam?keyword=' + object.keyword + '" target="_blank"><b>' + object.name + ' </b></a></td><td valign="top" align="right"><span class="fa fa-times-circle" onclick="fermer(event)" alt="fermer" style="cursor:pointer;"/></td></tr></table></div> ' + object.description;
    } else {
        var definition = '<div class="descriptiontitle"><table style="width:100%"><tr><td><b>' + object.name + ' </b></td><td valign="top" align="right"><span class="fa fa-times-circle" onclick="fermer(event)" alt="fermer" style="cursor:pointer;"/></td></tr></table></div> ' + object.description;
    }
    document.getElementById('gloss').innerHTML = definition;
    document.getElementById('frame').appendChild(document.getElementById('gloss'));
    displayGloss();
}

/////////////////////////////////////////////////////

//------------ Events ----------- //

//access to next IPs
function goNextIP(event) {
    timer.start();

    // suppression des div
    for (var i = min; i < max; i++) {
        document.getElementById("frame").removeChild(document.getElementById("ip_" + currentIpList[i]));
        document.getElementById("frame").removeChild(document.getElementById("edge:R:ip_" + currentIpList[i]));
    }

    if (currentIpList != null) {
        min = max;
        if (currentIpList.length - max > numIpMaxPerPage) {
            max += numIpMaxPerPage;
        } else {
            max = currentIpList.length;
        }
    }

    currentPage++;
    document.getElementById('gloss').style.visibility = "hidden";

    origin.children = new Array();
    displayIp(currentIpList);

    var t = setTimeout("timer.stop()", 10000);
}

//access to previous IPs
function goPreviousIP(event) {
    timer.start();

    // suppression des div
    for (var i = min; i < max; i++) {
        document.getElementById("frame").removeChild(document.getElementById("ip_" + currentIpList[i]));
        document.getElementById("frame").removeChild(document.getElementById("edge:R:ip_" + currentIpList[i]));
    }

    if (currentIpList != null) {
        max = min;
        min -= numIpMaxPerPage;
    }

    currentPage--;
    document.getElementById('gloss').style.visibility = "hidden";

    origin.children = new Array();
    displayIp(currentIpList);

    var t = setTimeout("timer.stop()", 10000);
}

//access to first IPs
function goFirstIP() {
    timer.start();

    // suppression des div
    for (var i = min; i < max; i++) {
        document.getElementById("frame").removeChild(document.getElementById("ip_" + currentIpList[i]));
        document.getElementById("frame").removeChild(document.getElementById("edge:R:ip_" + currentIpList[i]));
    }

    if (currentIpList != null) {
        max = numIpMaxPerPage;
        min = 0;
    }

    currentPage = 1;
    document.getElementById('gloss').style.visibility = "hidden";

    origin.children = new Array();
    displayIp(currentIpList);

    var t = setTimeout("timer.stop()", 10000);
}

//access to last IPs
function goLastIP() {
    timer.start();

    // suppression des div
    for (var i = min; i < max; i++) {
        document.getElementById("frame").removeChild(document.getElementById("ip_" + currentIpList[i]));
        document.getElementById("frame").removeChild(document.getElementById("edge:R:ip_" + currentIpList[i]));
    }

    if (currentIpList != null) {
        max = currentIpList.length;
        min = Math.floor(currentIpList.length / numIpMaxPerPage) * numIpMaxPerPage;
    }

    currentPage = Math.floor(currentIpList.length / numIpMaxPerPage) + 1;
    document.getElementById('gloss').style.visibility = "hidden";

    origin.children = new Array();
    displayIp(currentIpList);

    var t = setTimeout("timer.stop()", 10000);
}

//access to specific page IPs
function goSpecificPageIP(event) {
    timer.start();

    if (document.all) {
        var numPage = parseInt(event.srcElement.innerText);
    } else {
        var numPage = parseInt(event.target.textContent);
    }

    // suppression des div
    for (var i = min; i < max; i++) {
        document.getElementById("frame").removeChild(document.getElementById("ip_" + currentIpList[i]));
        document.getElementById("frame").removeChild(document.getElementById("edge:R:ip_" + currentIpList[i]));
    }

    min = numIpMaxPerPage * (numPage - 1);
    var minTmp = min + numIpMaxPerPage;

    if (minTmp <= currentIpList.length) {
        max = min + numIpMaxPerPage;
    } else {
        max = currentIpList.length;
    }

    currentPage = numPage;
    document.getElementById('gloss').style.visibility = "hidden";

    origin.children = new Array();
    displayIp(currentIpList);

    var t = setTimeout("timer.stop()", 10000);
}


//close description
function fermer() {
    document.getElementById('gloss').style.visibility = "hidden";
}

//onmouseout event
function onMouseOut() {
    // document.getElementById('gloss').style.visibility="hidden";
    timer.start();
    var t = setTimeout("timer.stop()", 5000);
}

//onclick on origin events
function loadDom(event) {
    if (Seam.Remoting.loadingMsgDiv.style.visibility == 'hidden') {
        document.getElementById("gloss").style.visibility = "hidden";

        // IE
        if (document.all) {
            var domKeyword = encodeURI(event.srcElement.innerText);

            document.location.href = "?dom=" + domKeyword;
            // other browser
        } else {
            var domKeyword = encodeURI(event.target.textContent);

            escape(document.location.href = "?dom=" + domKeyword);
        }
    }
}

function loadIP(event) {
    if (Seam.Remoting.loadingMsgDiv.style.visibility == 'hidden') {
        document.getElementById("gloss").style.visibility = "hidden";

        if (document.all) {
            document.location.href = "?dom=" + thisDomain + "?ip=" + event.srcElement.innerText;
        } else {
            document.location.href = "?dom=" + thisDomain + "?ip=" + event.target.textContent;
        }
    }
}

function loadActor(event) {
    if (Seam.Remoting.loadingMsgDiv.style.visibility == 'hidden') {
        document.getElementById("gloss").style.visibility = "hidden";

        if (document.all) {
            document.location.href = "?dom=" + thisDomain + "?ip=" + thisIp + "?actor=" + event.srcElement.innerText;
        } else {
            document.location.href = "?dom=" + thisDomain + "?ip=" + thisIp + "?actor=" + event.target.textContent;
        }
    }
}

function loadTrans(event) {
    if (Seam.Remoting.loadingMsgDiv.style.visibility == 'hidden') {
        document.getElementById("gloss").style.visibility = "hidden";

        if (document.all) {
            document.location.href = "?dom=" + thisDomain + "?ip=" + thisIp + "?actor=" + thisActor + "?trans=" + event.srcElement.innerText;
        } else {
            document.location.href = "?dom=" + thisDomain + "?ip=" + thisIp + "?actor=" + thisActor + "?trans=" + event.target.textContent;
        }
    }
}

//onmouseover events
function domainOnMouseOver(event) {

    if (document.all) {
        var domainKeyword = event.srcElement.innerText;
        var divDomNode = event.srcElement.offsetParent;
    } else {
        var domainKeyword = event.target.textContent;
        var divDomNode = event.target.offsetParent;
    }

    context = new Object();
    context.obj = divDomNode;

    currentObject = "domain";

    searchObject(domainKeyword);

    if (objectIndex != -1) {
        objectCallback(tableDescriptions[objectIndex]);
    } else {
        iheConcepts.getDomainByKeyword(domainKeyword, objectCallback);
    }
}

function ipOnMouseOver(event) {

    if (document.all) {
        var ipKeyword = event.srcElement.innerText;
        var divIPNode = event.srcElement.offsetParent;
    } else {
        var ipKeyword = event.target.textContent;
        var divIPNode = event.target.offsetParent;
    }

    context = new Object();
    context.obj = divIPNode;

    currentObject = "profile";

    searchObject(ipKeyword);

    if (objectIndex != -1) {
        objectCallback(tableDescriptions[objectIndex]);
    } else {
        iheConcepts.getIntegrationProfileByKeyword(ipKeyword, objectCallback);
    }
}

function actorOnMouseOver(event) {

    if (document.all) {
        var actorKeyword = event.srcElement.innerText;
        var divActorNode = event.srcElement.offsetParent;
    } else {
        var actorKeyword = event.target.textContent;
        var divActorNode = event.target.offsetParent;
    }

    context = new Object();
    context.obj = divActorNode;

    currentObject = "actor";

    searchObject(actorKeyword);

    if (objectIndex != -1) {
        objectCallback(tableDescriptions[objectIndex]);
    } else {
        iheConcepts.getActorByKeyword(actorKeyword, objectCallback);
    }
}

function transactionOnMouseOver(event) {

    if (document.all) {
        var transKeyword = event.srcElement.innerText;
        var divTransNode = event.srcElement.offsetParent;
    } else {
        var transKeyword = event.target.textContent;
        var divTransNode = event.target.offsetParent;
    }

    context = new Object();
    context.obj = divTransNode;

    currentObject = "transaction";

    searchObject(transKeyword);

    if (objectIndex != -1) {
        objectCallback(tableDescriptions[objectIndex]);
    } else {
        iheConcepts.getTransactionByKeyword(transKeyword, objectCallback);
    }
}

function ipoOnMouseOver(event) {

    if (document.all) {
        var ipoKeyword = event.srcElement.innerText;
        var divIPONode = event.srcElement.offsetParent;
    } else {
        var ipoKeyword = event.target.textContent;
        var divIPONode = event.target.offsetParent;
    }

    context = new Object();
    context.obj = divIPONode;

    currentObject = undefined;

    searchObject(ipoKeyword);

    if (objectIndex != -1) {
        objectCallback(tableDescriptions[objectIndex]);
    } else {
        iheConcepts.getIntegrationProfileOptionByKeyword(ipoKeyword, objectCallback);
    }
}

function bubbleOnMouseOver(event) {
    if (document.all) {
        var text = event.srcElement.alt;
        var divNode = event.srcElement.offsetParent;
    } else {
        var text = event.target.alt;
        var divNode = event.target.offsetParent;
    }

    context = new Object();
    context.obj = divNode;
    context.name = text;

    var definition = '<b>' + context.name + '</b>';
    document.getElementById('gloss').innerHTML = definition;
    displayGloss();
}

/////////////////////////////////////////////////////

function displayIp(ipList) {

    document.getElementById("datascroller").style.visibility = "visible";

    for (var i = min; i < max; i++) {
        var ipNode = origin.addChild(new TreeNode("ip_" + ipList[i], 110, angle, parseInt(FRAME_WIDTH / 2), parseInt(FRAME_HEIGHT / 2)));

        ipNode['fan_angle'] += 4 * ipNode['children'].length;
        ipNode['domNode'].innerHTML = "<a href='#' onclick='loadIP(event);' onmouseover='javascript:ipOnMouseOver(event);' onmouseout='javascript:onMouseOut();' >" + ipList[i] + "</a>";
        ipNode.updateChildren();
        document.getElementById("frame").appendChild(ipNode['domNode']);
    }

    if (min != 0) {
        document.getElementById("first").className = "rich-datascr-button";
        document.getElementById("previous").className = "rich-datascr-button";

        if (document.all) {
            document.getElementById("first").attachEvent('onclick', goFirstIP);
            document.getElementById("previous").attachEvent('onclick', goPreviousIP);
        } else {
            document.getElementById("first").addEventListener("click", goFirstIP, false);
            document.getElementById("previous").addEventListener("click", goPreviousIP, false);
        }
    } else {
        document.getElementById("first").className = "rich-datascr-button-dsbld rich-datascr-button";
        document.getElementById("previous").className = "rich-datascr-button-dsbld rich-datascr-button";

        if (document.all) {
            document.getElementById("first").detachEvent('onclick', goFirstIP);
            document.getElementById("previous").detachEvent('onclick', goPreviousIP);
        } else {
            document.getElementById("first").removeEventListener("click", goFirstIP, false);
            document.getElementById("previous").removeEventListener("click", goPreviousIP, false);
        }
    }

    if (max != ipList.length) {
        document.getElementById("last").className = "rich-datascr-button";
        document.getElementById("next").className = "rich-datascr-button";

        if (document.all) {
            document.getElementById("last").attachEvent('onclick', goLastIP);
            document.getElementById("next").attachEvent('onclick', goNextIP);
        } else {
            document.getElementById("last").addEventListener("click", goLastIP, false);
            document.getElementById("next").addEventListener("click", goNextIP, false);
        }
    } else {
        document.getElementById("last").className = "rich-datascr-button-dsbld rich-datascr-button";
        document.getElementById("next").className = "rich-datascr-button-dsbld rich-datascr-button";

        if (document.all) {
            document.getElementById("last").detachEvent('onclick', goLastIP);
            document.getElementById("next").detachEvent('onclick', goNextIP);
        } else {
            document.getElementById("last").removeEventListener("click", goLastIP, false);
            document.getElementById("next").removeEventListener("click", goNextIP, false);
        }
    }

    if (currentPage != -1) {
        for (var i = 0; i <= Math.floor(ipList.length / numIpMaxPerPage); i++) {
            var cell = document.getElementById("ds_table").rows[0].cells[3 + i];

            if (i + 1 == currentPage) {
                cell.className = "rich-datascr-act";
            } else {
                cell.className = "rich-datascr-inact";
            }
        }
    }
}

function displayGloss() {
    var target = context.obj;
    if (target) {
        document.getElementById('gloss').style.left = (parseInt(target.style.left) - 210) + "px";
        document.getElementById('gloss').style.top = (parseInt(target.style.top) - 10) + "px";
    }

    document.getElementById('gloss').style.visibility = "visible";
    timer.stop();
}

function searchObject(objectKeyword) {
    objectIndex = -1;
    for (var i = 0; i < tableDescriptions.length; i++) {
        if (tableDescriptions[i].keyword == objectKeyword) {
            objectIndex = i;
        }
    }
}

function createOrigin(text) {
    origin = new TreeNode("R", 100, 360, FRAME_WIDTH / 2, FRAME_HEIGHT / 2);
    origin['domNode'].innerHTML = '<div style="font-family:Arial,sans-serif;font-size:16px;font-weight:bold">' + text + '</div>';
    origin['domNode'].style.left = (parseInt(FRAME_WIDTH / 2) - parseInt(origin['domNode']['offsetWidth'] / 2) + FRAME_LEFT) + 'px';
    origin['domNode'].style.top = (parseInt(FRAME_HEIGHT / 2) - parseInt(origin['domNode']['offsetHeight'] / 2) + FRAME_TOP) + 'px';
    document.getElementById("frame").appendChild(origin['domNode']);
    graph.setOrigin(origin);
}

var DistributionListener = function () {

    this['origin_weight'] = 0;

    this['update'] = function () {
        if (this['origin_weight'] != origin['branch_weight']) {
            this['origin_weight'] = origin['branch_weight'];
            this.distributeChildren(origin);
        }
    };

    this['distributeChildren'] = function (node) {
        this.distribute(node['children']);
        //dumpArray( node['children'] );
        node.updateChildren();
        for (var i = 0; i < node['children'].length; i++) {
//			this.distributeChildren( node['children'][i] );
        }
    };

    this['distribute'] = function (list) {

        var length = list.length;

        list.sort(this['compare']);
        for (var i = 0; i < list.length - 1; i++) {
            if (i % 2 == 0) {
                var t = list.splice((list.length - 1), 1)[0];
                list.splice(i, 0, t);
            }
        }

        var k = 0;
        var pair_middle = parseInt(list.length / 2);
        if (pair_middle % 2 != 0) {
            pair_middle++;
        }
        for (var i = 0; i < pair_middle && (pair_middle + k) < (list.length - 1); i++) {
            if (i % 2 != 0) {
                var j = i * 2;
                var t1 = list[j];
                var t2 = list[j + 1];
                list[j] = list[pair_middle + k];
                list[j + 1] = list[pair_middle + k + 1];
                list[pair_middle + k] = t1;
                list[pair_middle + k + 1] = t2;
                k += 4;
            }
        }

        return ( list );
    }


    this['compare'] = function (a, b) {
        return ( (b['branch_weight']) - (a['branch_weight']) );
    };

}