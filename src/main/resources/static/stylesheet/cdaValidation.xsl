<?xml version='1.0'?>
<!DOCTYPE xsl:stylesheet [
<!ENTITY CDA-Stylesheet
'-//HL7//XSL HL7 V1.1 CDA Stylesheet: 2000-08-03//EN'>
]>
<xsl:stylesheet version='1.0'
    xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:ax21="http://validation.hitsp.nist.gov/xsd" xmlns:ns="http://validation.hitsp.nist.gov">
    <xsl:output method='html' indent='yes' version='4.01'
    doctype-public='-//W3C//DTD HTML 4.01//EN'/>
    
<xsl:template match='/'>
   <html>
       <head></head>
       <body>
           
           <h1>CDA Validation Result</h1>
           
           <div id="content">        
               <h2>Validation Service Information</h2>     
               <table>
                   <tr>
                       <td>Validation Date</td>
                       <td><xsl:value-of select="ax21:validationDate/text()"/></td>
                   </tr>
                   <tr>
                       <td>Validation Test</td>
                       <td><xsl:value-of select="ax21:validationTest/text()"/></td>
                   </tr>
                   <tr>
                       <td>Validation Time</td>
                       <td><xsl:value-of select="ax21:validationTime/text()"/></td>
                   </tr>
                   </table>
                 
                   <h2>Validation Test Information</h2>
               <table border="1">
                   <tr>
                       <td>Specification</td>
                       <td>Severity</td>
                       <td>Message</td>
                   </tr>
               <xsl:apply-templates select="*"/>
               </table>         
           </div>
       </body>
   </html>
    
</xsl:template>
 
    <xsl:template match="ax21:issue">

            <tr >
                <td><xsl:value-of select="ax21:specification"/></td>
                <td><xsl:value-of select="ax21:severity"/></td>
                <td><xsl:value-of select="ax21:message"/></td>
            </tr>
  
    </xsl:template>
    
 
    
</xsl:stylesheet>
