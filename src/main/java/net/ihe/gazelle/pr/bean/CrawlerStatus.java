package net.ihe.gazelle.pr.bean;

public enum CrawlerStatus {

	UNREACHABLE,

	NOT_MATCHABLE,

	MISMATCH,

	MATCH;

}
