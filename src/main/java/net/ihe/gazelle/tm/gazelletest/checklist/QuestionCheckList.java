package net.ihe.gazelle.tm.gazelletest.checklist;

public enum QuestionCheckList {

	TEST(1, "test", "tests"),
	METATEST(1, "metatest", "metatests"),
	TEST_INSTANCE(1, "testinstance", "test instances"),
	ROLE_IN_TEST(1, "roleintest", "role in test"),
	LIST_TEST_STEPS_NOT_ASSIGNED_TO_TEST(2, "listtsnotassigned", "gazelle.testmanagement.checklist.tsnotassigned"),
	LIST_TEST_WITH_NO_STEPS(2, "listtestwithnosteps", "gazelle.testmanagement.checklist.listtestwithnosteps"),
	LIST_CI_WITH_NO_TESTSTEPS(2, "listciwithnots", "gazelle.testmanagement.checklist.listciwithnots"),
	LIST_AIPO_WITH_NO_TEST(2, "listaipowithnotest", "gazelle.testmanagement.checklist.listaipowithnotest"),
	LIST_AIPO_WITHOUT_CONNECTATHON_TEST(2, "listaipowithoutconnectathontest", "gazelle.testmanagement.checklist.listaipowithoutConnectathontest"),
	LIST_AIPO_WITHOUT_PRE_CONNECTATHON_TEST(2, "listaipowithoutpreconnectathontest", "gazelle.testmanagement.checklist.listaipowithoutPreConnectathontest"),
	METATEST_WITH_NO_TEST(2, "metatestwithnotest", "gazelle.testmanagement.checklist.metatestwithnotest"),
	TEST_STEPS_INSTANCES_WITH_NO_TEST_INSTANCES(2, "tsiwithnoti", "gazelle.testmanagement.checklist.tsiwithnoti"),
	LIST_CII_WITH_NO_TSI(2, "ciiwithnotsi", "gazelle.testmanagement.checklist.ciiwithnotsi"),
	LIST_TI_WITH_NO_TS(2, "ltiwithnots", "gazelle.testmanagement.checklist.ltiwithnots"),
	LIST_TI_WITH_NO_TIP(2, "ltiwithnotip", "gazelle.testmanagement.checklist.ltiwithnotip"),
	ROLE_IN_TEST_WITH_NO_TESTS(2, "ritwithnotest", "gazelle.testmanagement.checklist.ritwithnotest"),
	ROLE_IN_TEST_WITH_NO_TESTPARTICIPANTS(2, "ritwithnotestparticipants", "gazelle.testmanagement.checklist.ritwithnotestparticipants");

	private int level;

	private String identifier;

	private String question;

	QuestionCheckList(int i, String s, String question) {
		this.level = i;
		this.identifier = s;
		this.question = question;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

}
