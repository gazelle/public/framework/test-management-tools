/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.financial;

import net.ihe.gazelle.tm.financial.model.Invoice;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import net.ihe.gazelle.users.model.Institution;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;

/**
 * <b>Class Description : </b>FinancialSummary<br>
 * <br>
 * This class represents the financial information for a given institution
 *
 * @author Jean-Renan Chatel (INRIA, FR) - JB Meyer(INRIA, FR)
 * @version 1.0 - 2008, July 15
 * @class FinancialSummary.java
 * @package net.ihe.gazelle.tm.financial.model
 */
public class FinancialSummary implements Serializable {

    private static final long serialVersionUID = -2101614622954225301L;
    private Integer id;
    private List<FinancialSummaryOneSystem> financialSummaryOneSystems;
    private Invoice invoice;
    private Institution institution;
    private TestingSession testingSession;
    private Boolean hasASystemGotMissingDependencies = false;
    private String domains;
    private BigDecimal fee;
    private BigDecimal feeVAT;
    private BigDecimal feeDiscount;
    private Boolean isDiscount;
    private BigDecimal totalFeeToPay;
    private BigDecimal totalFeePaid;
    private BigDecimal totalFeeDue;
    private String reasonForDiscount;
    private NumberFormat nf;
    private boolean isDomainExist = true;
    private Integer numberParticipant;
    private String invoiceSpecialInstruction = null;
    private boolean displayMore = false;

    {
        numberParticipant = 0;
        totalFeeToPay = new BigDecimal("0.00");
        feeVAT = new BigDecimal("0.00");
    }

    public FinancialSummary() {
        nf = NumberFormat.getCurrencyInstance();
    }

    public String getInvoiceSpecialInstruction() {
        return invoiceSpecialInstruction;
    }

    public void setInvoiceSpecialInstruction(String invoiceSpecialInstruction) {
        this.invoiceSpecialInstruction = invoiceSpecialInstruction;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
        if (this.invoice != null) {
            this.invoiceSpecialInstruction = this.invoice.getSpecialInstructions();
        }
    }

    public TestingSession getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(TestingSession testingSession) {
        this.testingSession = testingSession;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public List<FinancialSummaryOneSystem> getFinancialSummaryOneSystems() {
        return financialSummaryOneSystems;
    }

    public void setFinancialSummaryOneSystems(List<FinancialSummaryOneSystem> financialSummaryOneSystems) {
        this.financialSummaryOneSystems = financialSummaryOneSystems;
    }

    public Integer getNumberParticipant() {
        return numberParticipant;
    }

    public void setNumberParticipant(Integer numberParticipant) {
        this.numberParticipant = numberParticipant;
    }

    public boolean isDomainExist() {
        return isDomainExist;
    }

    public void setDomainExist(boolean isDomainExist) {
        this.isDomainExist = isDomainExist;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDomains() {
        return domains;
    }

    public void setDomains(String domains) {
        this.domains = domains;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getTotalFeeToPay() {
        if (totalFeeToPay == null) {
            totalFeeToPay = new BigDecimal("0.00");
        }
        return totalFeeToPay;
    }

    public void setTotalFeeToPay(BigDecimal totalFeeToPay) {
        this.totalFeeToPay = totalFeeToPay;
    }

    public BigDecimal getTotalFeePaid() {
        return totalFeePaid;
    }

    public void setTotalFeePaid(BigDecimal totalFeePaid) {
        this.totalFeePaid = totalFeePaid;
    }

    public BigDecimal getTotalFeeDue() {
        return totalFeeDue;
    }

    public void setTotalFeeDue(BigDecimal totalFeeDue) {
        this.totalFeeDue = totalFeeDue;
    }

    public String getFeeAsCurrency(TestingSession selectedTestingSession) {
        String currency = selectedTestingSession.getCurrency().getKeyword();
        return currency + " " + fee;
    }

    public Boolean getHasASystemGotMissingDependencies() {
        return hasASystemGotMissingDependencies;
    }

    public void setHasASystemGotMissingDependencies(Boolean hasASystemGotMissingDependencies) {
        this.hasASystemGotMissingDependencies = hasASystemGotMissingDependencies;
    }

    public BigDecimal getFeeVAT() {
        if (feeVAT == null) {
            feeVAT = new BigDecimal("0.00");
        }
        return feeVAT;
    }

    public void setFeeVAT(BigDecimal feeVAT) {
        this.feeVAT = feeVAT;
    }

    public BigDecimal getFeeDiscount() {
        return feeDiscount;
    }

    public void setFeeDiscount(BigDecimal feeDiscount) {
        this.feeDiscount = feeDiscount;
    }

    public Boolean getIsDiscount() {
        return isDiscount;
    }

    public void setIsDiscount(Boolean isDiscount) {
        this.isDiscount = isDiscount;
    }

    public String getReasonForDiscount() {
        return reasonForDiscount;
    }

    public void setReasonForDiscount(String reasonForDiscount) {
        this.reasonForDiscount = reasonForDiscount;
    }

    public NumberFormat getNf() {
        return nf;
    }

    public void setNf(NumberFormat nf) {
        this.nf = nf;
    }

    /**
     * Returns the VAT amount to pay by the vendor with the currency : eg. EUR 15000.00
     *
     * @param selectedTestingSession current testing session of the logged user
     * @return feeVAT : VAT amount to pay
     */
    public String getFeeVATAsCurrency(TestingSession selectedTestingSession) {
        String currency = selectedTestingSession.getCurrency().getKeyword();
        return currency + " " + feeVAT;
    }

    /**
     * Returns the Discount amount to pay by the vendor with the currency : eg. EUR 15000.00
     *
     * @param selectedTestingSession current testing session of the logged user
     * @return feeDiscount : Discount amount to pay
     */
    public String getFeeDiscountAsCurrency(TestingSession selectedTestingSession) {
        String currency = testingSession.getCurrency().getKeyword();
        return currency + " " + feeDiscount;
    }

    /**
     * Returns the Additional fees to pay by the vendor with the currency : eg. EUR 15000.00
     *
     * @param selectedTestingSession current testing session of the logged user
     * @return feeDiscount : Discount amount to pay
     */
    public String getAdditionalFeeAsCurrency(TestingSession selectedTestingSession) {
        String currency = selectedTestingSession.getCurrency().getKeyword();
        return currency + " " + feeDiscount.negate();
    }

    /**
     * Returns the total amount to pay by the vendor with the currency : eg. EUR 15000.00
     *
     * @param selectedTestingSession current testing session of the logged user
     * @return totalFeeToPay : Amount to pay, with taxes included
     */
    public String getTotalFeeToPayAsCurrency(TestingSession selectedTestingSession) {
        String currency = testingSession.getCurrency().getKeyword();

        if (feeVAT == null) {
            return "Internal Error : feeVAT not calculated";
        }
        if (fee == null) {
            return "Internal Error : fee not calculated";
        }

        BigDecimal totalFeeToPay = feeVAT.add(fee);
        if (feeDiscount != null) {
            totalFeeToPay = totalFeeToPay.add(feeDiscount.negate());
        }
        return currency + " " + totalFeeToPay;
    }

    /**
     * Returns the total amount paid by the vendor with the currency : eg. EUR 15000.00
     *
     * @param selectedTestingSession current testing session of the logged user
     * @return totalFeePaid : Amount paid.
     */
    public String getTotalFeePaidAsCurrency(TestingSession selectedTestingSession) {
        String currency = selectedTestingSession.getCurrency().getKeyword();
        return currency + " " + totalFeePaid;
    }

    public String getSystemsFeesAsCurrency(TestingSession selectedTestingSession) {
        String currency = selectedTestingSession.getCurrency().getKeyword();
        BigDecimal res = new BigDecimal(0);
        for (FinancialSummaryOneSystem financialSummaryOneSystem : getFinancialSummaryOneSystems()) {
            res = res.add(BigDecimal.valueOf(financialSummaryOneSystem.getSystemFee().intValue()));
        }
        return currency + " " + res.setScale(2);
    }

    public String getParticipantsFeeAsCurrency(TestingSession selectedTestingSession) {
        String currency = selectedTestingSession.getCurrency().getKeyword();
        BigDecimal nbParticipants = BigDecimal.valueOf(getNumberAdditionalParticipant(selectedTestingSession));
        BigDecimal feeParticipant = BigDecimal.valueOf(selectedTestingSession.getFeeParticipant().intValue());
        BigDecimal res = nbParticipants.multiply(feeParticipant);
        return currency + " " + res.setScale(2);
    }

    /**
     * Returns the total amount due by the vendor with the currency : eg. EUR 15000.00
     *
     * @param selectedTestingSession current testing session of the logged user
     * @return totalFeeDue : Amount due.
     */
    public String getTotalFeeDueAsCurrency(TestingSession selectedTestingSession) {
        String currency = selectedTestingSession.getCurrency().getKeyword();
        return currency + " " + totalFeeDue;
    }

    public int getNumberTable() {
        int res = 0;
        int sysnum = 0;
        if (this.getFinancialSummaryOneSystems() != null) {
            sysnum = this.getFinancialSummaryOneSystems().size();
        }
        res = sysnum;
        if ((this.numberParticipant - (sysnum * 2)) > 0) {
            res = res + ((this.numberParticipant - (sysnum * 2)) / 2);
        }
        return res;
    }

    public int getNumberAdditionalParticipant(TestingSession activatedTestingSession) {
        int sysnum = 0;
        if (this.getFinancialSummaryOneSystems() != null) {
            sysnum = this.getFinancialSummaryOneSystems().size();
        }
        if ((this.numberParticipant - (sysnum * activatedTestingSession.getNbParticipantsIncludedInSystemFees())) > 0) {
            return (this.numberParticipant - (sysnum * activatedTestingSession.getNbParticipantsIncludedInSystemFees()));
        }
        return 0;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((domains == null) ? 0 : domains.hashCode());
        result = (prime * result) + ((institution == null) ? 0 : institution.hashCode());
        result = (prime * result) + ((testingSession == null) ? 0 : testingSession.hashCode());
        result = (prime * result) + ((totalFeeDue == null) ? 0 : totalFeeDue.hashCode());
        result = (prime * result) + ((totalFeePaid == null) ? 0 : totalFeePaid.hashCode());
        result = (prime * result) + ((totalFeeToPay == null) ? 0 : totalFeeToPay.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FinancialSummary other = (FinancialSummary) obj;
        if (domains == null) {
            if (other.domains != null) {
                return false;
            }
        } else if (!domains.equals(other.domains)) {
            return false;
        }
        if (institution == null) {
            if (other.institution != null) {
                return false;
            }
        } else if (!institution.equals(other.institution)) {
            return false;
        }
        if (testingSession == null) {
            if (other.testingSession != null) {
                return false;
            }
        } else if (!testingSession.equals(other.testingSession)) {
            return false;
        }
        if (totalFeeDue == null) {
            if (other.totalFeeDue != null) {
                return false;
            }
        } else if (!totalFeeDue.equals(other.totalFeeDue)) {
            return false;
        }
        if (totalFeePaid == null) {
            if (other.totalFeePaid != null) {
                return false;
            }
        } else if (!totalFeePaid.equals(other.totalFeePaid)) {
            return false;
        }
        if (totalFeeToPay == null) {
            if (other.totalFeeToPay != null) {
                return false;
            }
        } else if (!totalFeeToPay.equals(other.totalFeeToPay)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FinancialSummary [id=" + id + ", financialSummaryOneSystems=" + financialSummaryOneSystems
                + ", institution=" + institution + ", testingSession=" + testingSession
                + ", hasASystemGotMissingDependencies=" + hasASystemGotMissingDependencies + ", domains=" + domains
                + ", fee=" + fee + ", feeVAT=" + feeVAT + ", feeDiscount=" + feeDiscount + ", isDiscount=" + isDiscount
                + ", totalFeeToPay=" + totalFeeToPay + ", totalFeePaid=" + totalFeePaid + ", totalFeeDue="
                + totalFeeDue + ", reasonForDiscount=" + reasonForDiscount + ", nf=" + nf + ", isDomainExist="
                + isDomainExist + ", numberParticipant=" + numberParticipant + "]";
    }

    public boolean isDisplayMore() {
        if (getFinancialSummaryOneSystems() != null && getFinancialSummaryOneSystems().size() > 1) {
            setDisplayMore(true);
        }
        return displayMore;
    }

    public void setDisplayMore(boolean displayMore) {
        this.displayMore = displayMore;
    }
}
