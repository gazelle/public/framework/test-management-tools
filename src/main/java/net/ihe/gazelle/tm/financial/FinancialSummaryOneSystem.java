/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.financial;

import java.io.Serializable;
import java.math.BigDecimal;

import net.ihe.gazelle.tm.systems.model.SystemInSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <b>Class Description : </b>FinancialSummaryOneSystem<br>
 * <br>
 * This class represents the financial information for a system
 * 
 * 
 * @class FinancialSummaryOneSystem.java
 * @package net.ihe.gazelle.tm.financial.model
 * @author Sasi Suruliraj (MIR - USA) - Jean-Renan Chatel (INRIA, FR)- Jean-Baptiste Meyer (INRIA, FR)
 * @version 1.0 - 2008, October 24
 * 
 */
public class FinancialSummaryOneSystem implements Serializable {

	private static final long serialVersionUID = -1;

	/** entityManager is the interface used to interact with the persistence context. */

	private static Logger log = LoggerFactory.getLogger(FinancialSummaryOneSystem.class);

	private Integer id;

	private SystemInSession systemInSession;

	private FinancialSummary financialSummary;

	private String domainsForSystem;

	private BigDecimal systemFee = new BigDecimal(0);

	private Boolean hasMissingDependecies;

	public FinancialSummaryOneSystem() {
	}

	public SystemInSession getSystemInSession() {
		return systemInSession;
	}

	public void setSystemInSession(SystemInSession systemInSession) {
		this.systemInSession = systemInSession;
	}

	public FinancialSummary getFinancialSummary() {
		return financialSummary;
	}

	public void setFinancialSummary(FinancialSummary financialSummary) {
		this.financialSummary = financialSummary;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDomainsForSystem() {
		return domainsForSystem;
	}

	public void setDomainsForSystem(String domainsForSystem) {
		this.domainsForSystem = domainsForSystem;
	}

	public BigDecimal getSystemFee() {
		return systemFee;
	}

	public void setSystemFee(BigDecimal systemFee) {
		this.systemFee = systemFee;
	}

	public Boolean getHasMissingDependecies() {
		return hasMissingDependecies;
	}

	public void setHasMissingDependecies(Boolean hasMissingDependecies) {
		this.hasMissingDependecies = hasMissingDependecies;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((domainsForSystem == null) ? 0 : domainsForSystem.hashCode());
		result = (prime * result) + ((hasMissingDependecies == null) ? 0 : hasMissingDependecies.hashCode());
		result = (prime * result) + ((systemFee == null) ? 0 : systemFee.hashCode());
		result = (prime * result) + ((systemInSession == null) ? 0 : systemInSession.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FinancialSummaryOneSystem other = (FinancialSummaryOneSystem) obj;
		if (domainsForSystem == null) {
			if (other.domainsForSystem != null) {
				return false;
			}
		} else if (!domainsForSystem.equals(other.domainsForSystem)) {
			return false;
		}
		if (hasMissingDependecies == null) {
			if (other.hasMissingDependecies != null) {
				return false;
			}
		} else if (!hasMissingDependecies.equals(other.hasMissingDependecies)) {
			return false;
		}
		if (systemFee == null) {
			if (other.systemFee != null) {
				return false;
			}
		} else if (!systemFee.equals(other.systemFee)) {
			return false;
		}
		if (systemInSession == null) {
			if (other.systemInSession != null) {
				return false;
			}
		} else if (!systemInSession.equals(other.systemInSession)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FinancialSummaryOneSystem [id=" + id + ", systemInSession=" + systemInSession + ", domainsForSystem="
				+ domainsForSystem + ", systemFee=" + systemFee + ", hasMissingDependecies=" + hasMissingDependecies
				+ "]";
	}

}
