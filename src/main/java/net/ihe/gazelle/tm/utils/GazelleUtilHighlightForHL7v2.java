package net.ihe.gazelle.tm.utils;

import java.util.ArrayList;
import java.util.List;

public class GazelleUtilHighlightForHL7v2 {

	public static String highlightForHL7v2Message(String input) {
		String messageContent = input;

		Integer b = 0;
		List<String> listSegment = new ArrayList<String>();
		listSegment.clear();

		Integer a = messageContent.indexOf("|");
		messageContent = messageContent.replace("<", "&lt;");
		messageContent = messageContent.replace(">", "&gt;");

		if ((a < messageContent.length()) && (a != -1)) {
			String subString = messageContent.substring(b, a);
			messageContent = messageContent.replaceFirst(subString,
					("<span class=\"hl-reserved\">" + subString + "</span>"));

			while ((a < messageContent.length()) && (a > 0) && (b >= 0)) {
				a = messageContent.indexOf("\r", a + 1);

				if (((a + 1) < messageContent.length()) && (a != -1)) {
					b = messageContent.indexOf("|", a + 1);

					if ((b - a) == 4) {
						subString = messageContent.substring(a, b);

						if (!listSegment.contains(subString)) {
							listSegment.add(subString);
							messageContent = messageContent.replaceAll(subString, ("\r<span class=\"hl-reserved\">"
									+ subString.substring(1, 4) + "</span>"));
						}
					}
				} else {
					a = -1;
				}
			}

		}

		messageContent = messageContent.replace("&", "<span class=\"hl-special\">&amp;</span>");
		messageContent = messageContent.replace("~", "<span class=\"hl-special\">&tilde;</span>");
		messageContent = messageContent.replace("#", "<span class=\"hl-special\">&#35;</span>");
		messageContent = messageContent.replace("|", "<span class=\"hl-special\">&#124;</span>");
		messageContent = messageContent.replace("^", "<span class=\"hl-brackets\">&#94;</span>");
		messageContent = messageContent.replace("\r", "<span class=\"hl-crlf\">[CR]</span><br/>");

		return messageContent;
	}

}
