/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.tm.utils.systems;

import net.ihe.gazelle.tf.model.Actor;
import net.ihe.gazelle.tf.model.Domain;
import net.ihe.gazelle.tf.model.IntegrationProfile;
import net.ihe.gazelle.tf.model.IntegrationProfileOption;
import net.ihe.gazelle.tm.systems.model.TestingDepth;

/**
 * <b>Class Description : </b>IHEImplementationForSystem<br>
 * <br>
 * This class describes an IHE implementation for a System. A IHEImplementationForSystem corresponds to a combination of one domain/actor/integration profile/options implemented. An integration
 * Statement declares one or many IHE implementations (list of domain/actor/integration profile/options combinations. These combinations are associated to a system. This class belongs to the
 * ProductRegistry module. This class is NOT an ENTITY, there is no IHEImplementationForSystem table in the database.
 * 
 * IHEImplementationForSystem possesses the following attributes :
 * <ul>
 * <li><b>domain</b> : Domain implemented by the system (associated with the other attributes)</li>
 * <li><b>integrationProfile</b> : Integration Profile implemented by the system (associated with the other attributes)</li>
 * <li><b>actor</b> : Actor implemented by the system (associated with the other attributes)</li>
 * <li><b>integrationProfileOption</b> : Integration Profile Option to search (associated with the other attributes)</li>
 * </ul>
 * </br> <b>Example of IHEImplementationForSystem</b> : Association between (RAD, SWF, EC, CATH_EVIDENCE)<br>
 * 
 * 
 * @class IHEImplementationForSystem.java
 * @package net.ihe.gazelle.pr.systems.model
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 * @version 1.0 - 2008, February 20
 * 
 */

public class IHEImplementationForSystem implements java.io.Serializable, Comparable<IHEImplementationForSystem> {

	/** Serial ID version of this object */
	private static final long serialVersionUID = -450911165438283760L;

	// Attributes

	private Integer id;
	private Domain domain;
	private IntegrationProfile integrationProfile;
	private Actor actor;
	private IntegrationProfileOption integrationProfileOption;
	private TestingDepth testingDepth;
	private TestingDepth wantedTestingDepth;
	private boolean testingDepthReviewed;

	// Constructor
	public IHEImplementationForSystem(Domain domain, IntegrationProfile integrationProfile, Actor actor,
			IntegrationProfileOption integrationProfileOption) {
		super();
		this.domain = domain;
		this.integrationProfile = integrationProfile;
		this.actor = actor;
		this.integrationProfileOption = integrationProfileOption;
	}

	// Constructor
	public IHEImplementationForSystem(IntegrationProfile integrationProfile, Actor actor,
			IntegrationProfileOption integrationProfileOption) {
		super();
		this.integrationProfile = integrationProfile;
		this.actor = actor;
		this.integrationProfileOption = integrationProfileOption;
	}

	// Constructor
	public IHEImplementationForSystem(Domain domain, IntegrationProfile integrationProfile, Actor actor,
									  IntegrationProfileOption integrationProfileOption, TestingDepth testingDepth, TestingDepth wantedTestingDepth,
									  boolean testingDepthReviewed) {
		super();
		this.domain = domain;
		this.integrationProfile = integrationProfile;
		this.actor = actor;
		this.integrationProfileOption = integrationProfileOption;
		this.testingDepth = testingDepth;
		this.wantedTestingDepth = wantedTestingDepth;
		this.testingDepthReviewed = testingDepthReviewed;
	}

	// Constructor
	public IHEImplementationForSystem(IntegrationProfile integrationProfile, Actor actor,
									  IntegrationProfileOption integrationProfileOption, TestingDepth testingDepth, TestingDepth wantedTestingDepth,
									  boolean testingDepthReviewed) {
		super();
		this.integrationProfile = integrationProfile;
		this.actor = actor;
		this.integrationProfileOption = integrationProfileOption;
		this.testingDepth = testingDepth;
		this.wantedTestingDepth = wantedTestingDepth;
		this.testingDepthReviewed = testingDepthReviewed;
	}

	public IHEImplementationForSystem(IHEImplementationForSystem inIHEImplementationForSystem) {
		this.integrationProfile = inIHEImplementationForSystem.getIntegrationProfile();
		this.actor = inIHEImplementationForSystem.getActor();
		this.integrationProfileOption = inIHEImplementationForSystem.getIntegrationProfileOption();
		this.testingDepth = inIHEImplementationForSystem.getTestingDepth();
	}

	// *********************************************************
	// Getters and Setters : setup the object properties
	// *********************************************************

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public IntegrationProfile getIntegrationProfile() {
		return integrationProfile;
	}

	public void setIntegrationProfile(IntegrationProfile integrationProfile) {
		this.integrationProfile = integrationProfile;
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public IntegrationProfileOption getIntegrationProfileOption() {
		return integrationProfileOption;
	}

	public void setIntegrationProfileOption(IntegrationProfileOption integrationProfileOption) {
		this.integrationProfileOption = integrationProfileOption;
	}

	public TestingDepth getTestingDepth() {
		return testingDepth;
	}

	public void setTestingDepth(TestingDepth testingDepth) {
		this.testingDepth = testingDepth;
	}

	public TestingDepth getWantedTestingDepth() {
		return wantedTestingDepth;
	}

	public void setWantedTestingDepth(TestingDepth wantedTestingDepth) {
		this.wantedTestingDepth = wantedTestingDepth;
	}

	public boolean isTestingDepthReviewed() {
		return testingDepthReviewed;
	}

	public void setTestingDepthReviewed(boolean testingDepthReviewed) {
		this.testingDepthReviewed = testingDepthReviewed;
	}

	@Override
	public int compareTo(IHEImplementationForSystem o) {
		if (getIntegrationProfile().getKeyword().compareTo(o.getIntegrationProfile().getKeyword()) < 0) {
			return -1;
		} else if (getIntegrationProfile().getKeyword().compareTo(o.getIntegrationProfile().getKeyword()) > 0) {
			return 1;
		} else {
			if (getActor().getKeyword().compareTo(o.getActor().getKeyword()) <= 0) {
				return -1;
			} else {
				return 1;
			}
		}
	}

	@Override
	public String toString() {
		return getIntegrationProfile().getKeyword() + "|" + getActor().getKeyword() + "|"
				+ getIntegrationProfileOption();
	}

}
