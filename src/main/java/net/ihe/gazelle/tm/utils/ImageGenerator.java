package net.ihe.gazelle.tm.utils;

import gui.ava.html.image.generator.HtmlImageGenerator;

import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;

public class ImageGenerator {

	/**
	 * use of html2image-0.9
	 */
	public static void exportToOutputStream(OutputStream os, String htmlContent) throws IOException {
		HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
		imageGenerator.loadHtml("<div style=\"width:1000px;font-size:14;\">" + htmlContent + "</div>");
		ImageIO.write(imageGenerator.getBufferedImage(), "PNG", os);
	}

}
