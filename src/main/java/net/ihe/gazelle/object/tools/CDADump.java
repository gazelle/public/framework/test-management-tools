/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.object.tools;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import org.jdom.JDOMException;
import org.jdom.ProcessingInstruction;
import org.jdom.input.DOMBuilder;
import org.jdom.output.DOMOutputter;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.FileOutputStream;

/**
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 *
 */
public class CDADump {

	private static Logger log = LoggerFactory.getLogger(CDADump.class);

	public CDADump() {
	}

	public static void updateStyleSheetOfCDAFile(String cdapath) {
		CDADump.updateStyleSheetOfAfile(cdapath, CDADump.getStyleSheetUrl());
	}

	public static void updateStyleSheetOfAfile(String filepath, String urlOfFile) {
		try {
			org.jdom.Document newDoc = CDADump.getCDADocument(filepath);
			CDADump.updateStyleSheetOfADocument(newDoc, urlOfFile);
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(newDoc, new FileOutputStream(filepath));
		} catch (Exception e) {
			log.error("", e);
		}
	}

	public static void updateStyleSheetOfADocument(org.jdom.Document newDoc, String urlOfFile) {
		ProcessingInstruction e_proc = new ProcessingInstruction("xml-stylesheet", "href=\"" + urlOfFile
				+ "\" type=\"text/xsl\"");
		newDoc.addContent(0, e_proc);
	}

	public static org.jdom.Document getCDADocument(String cdapath) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			java.io.File xmlFile = new java.io.File(cdapath);
			Document document = builder.parse(xmlFile);
			org.jdom.Document newDoc = CDADump.DOMtoJDOM(document);
			return newDoc;
		} catch (Exception e) {
			e.getStackTrace();
			return null;
		}
	}

	private static String getStyleSheetUrl() {
		String ApplBaseName = ApplicationPreferenceManagerImpl.instance().getApplicationUrl();
		String result = ApplBaseName + "stylesheet/cda.xsl";
		return result;
	}

	public static String getStyleSheetUrlForValidation() {
		String result = "";
		String ApplBaseName = ApplicationPreferenceManagerImpl.instance().getApplicationUrl();
		result = ApplBaseName + "stylesheet/cdaValidation.xsl";
		return result;
	}

	public static org.jdom.Document DOMtoJDOM(org.w3c.dom.Document documentDOM) {
		try {
			DOMBuilder builder = new DOMBuilder();
			org.jdom.Document documentJDOM = builder.build(documentDOM);
			return documentJDOM;
		} catch (Exception e) {
			e.getStackTrace();
			return null;
		}
	}

	public static org.w3c.dom.Document JDOM2DOM(org.jdom.Document jdomDoc) {
		org.w3c.dom.Document domDoc = null;
		DOMOutputter outputter = new DOMOutputter();
		try {
			domDoc = outputter.output(jdomDoc);
		} catch (JDOMException e) {
			log.error("", e);
		}
		return domDoc;
	}

	public static String convertDocumentToString(org.jdom.Document doc) {
		XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
		return outputter.outputString(doc);
	}

}
