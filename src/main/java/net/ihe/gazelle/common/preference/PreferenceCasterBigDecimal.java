/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.common.preference;

import java.math.BigDecimal;

class PreferenceCasterBigDecimal implements PreferenceCaster<BigDecimal> {
	@Override
	public BigDecimal asObject(String stringValue) {
		if (stringValue == null) {
			return new BigDecimal(0);
		}
		return new BigDecimal(stringValue);
	}

	@Override
	public String asString(BigDecimal value) {
		if (value == null) {
			return "0";
		}
		return value.toString();
	}

	@Override
	public String getPreferenceClassName() {
		return BigDecimal.class.getCanonicalName();
	}
}