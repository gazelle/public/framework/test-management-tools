package net.ihe.gazelle.common.util;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.log.ExceptionLogging;
import net.ihe.gazelle.tm.gazelletest.model.instance.TestInstancePathToLogFile;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.FileTypeMap;
import javax.ejb.Remove;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

import static org.jboss.seam.ScopeType.SESSION;

/**
 * <b>Class Description : </b>DocumentFileUpload<br>
 * <br>
 * This class manages the upload actions of Documents/Files . It corresponds to the Business Layer. It is used by all Richfaces listener components
 * : <rich:fileUpload ... >
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, March 4th
 * @class DocumentFileUpload.java
 * @package net.ihe.gazelle.common.util
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

@Name("documentFileUpload")
@Scope(SESSION)
@GenerateInterface("DocumentFileUploadLocal")
public class DocumentFileUpload implements DocumentFileUploadLocal, Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450973131509093760L;

    private static Logger log = LoggerFactory.getLogger(DocumentFileUpload.class);

    public static String getGazelleLogReturnPath() {
        ApplicationPreferenceManagerImpl.instance();
        return ApplicationPreferenceManagerImpl.instance().getGazelleDataPath() + java.io.File.separatorChar
                + ApplicationPreferenceManagerImpl.instance().getStringValue("log_return_path");
    }

    public static void showFile(String fullPath) {
        DocumentFileUpload.showFile(fullPath, null, false);
    }

    public static void showFile(String fullPath, String filename, boolean download) {
        InputStream inputStream = null;
        try {
            java.io.File f = new java.io.File(fullPath);
            if (filename == null) {
                filename = f.getName();
            }
            if (f.exists()) {
                inputStream = new FileInputStream(fullPath);
                showFile(inputStream, filename, download);
                inputStream.close();
            } else {
                log.warn("File at {} does not exist", filename);
            }
        } catch (Exception e) {
            ExceptionLogging.logException(e, log);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to display file : " + e.getMessage());
            IOUtils.closeQuietly(inputStream);
        }
    }

    public static void showFile(File file, boolean download) throws IOException {
        showFile(file.getInputStream(), file.getName(), download);
    }

    public static void showFile(byte[] bytes, String filename, boolean download) throws IOException {
        showFile(new ByteArrayInputStream(bytes), filename, download);
    }

    public static void showFile(InputStream inputStream, String filename, boolean download) throws IOException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        try {
            response = (HttpServletResponse) PropertyUtils.getProperty(response, "response");
        } catch (Exception e1) {
            // nothing to do
            // we are at org.apache.catalina.connector.ResponseFacade
        }
        response.reset();
        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();

        showFile(request, response, inputStream, filename, download);
        if (inputStream != null) {
            inputStream.close();
        }
        facesContext.responseComplete();
    }

    public static void showFile(HttpServletRequest request, HttpServletResponse response, InputStream inputStream,
                                String filename, boolean download) {
        try {
            if (inputStream != null && filename != null) {
                String userAgent = request.getHeader("user-agent");
                boolean isInternetExplorer = (userAgent.contains("MSIE"));

                int length = inputStream.available();

                if (filename.toLowerCase().endsWith(".pdf")) {
                    response.setContentType("application/pdf");
                } else if (filename.toLowerCase().endsWith(".xml")) {
                    response.setContentType("text/xml");
                } else {
                    String contentType = FileTypeMap.getDefaultFileTypeMap().getContentType(filename);
                    response.setContentType(contentType);
                }

                byte[] fileNameBytes = filename.getBytes((isInternetExplorer) ? ("windows-1250") : ("utf-8"));
                StringBuilder dispositionFileName = new StringBuilder();
                for (byte b : fileNameBytes) {
                    dispositionFileName.append((char) (b & 0xff));
                }

                String disposition;

                if (download) {
                    disposition = "attachment; filename=\"" + dispositionFileName + "\"";
                } else {
                    disposition = "inline; filename=\"" + dispositionFileName + "\"";
                }

                response.setHeader("Content-disposition", disposition);

                response.setContentLength(length);

                ServletOutputStream servletOutputStream = response.getOutputStream();

                IOUtils.copy(inputStream, servletOutputStream);

                servletOutputStream.flush();
                servletOutputStream.close();

                inputStream.close();
            }
        } catch (Exception e) {
            ExceptionLogging.logException(e, log);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to display file : " + e.getMessage());
        }
    }

    @Override
    public void showFile(TestInstancePathToLogFile testInstancePathToLogFile) {
        String fullPath = getGazelleLogReturnPath() + java.io.File.separatorChar + testInstancePathToLogFile.getPath();
        showFile(fullPath);
    }

    @Override
    @Destroy
    @Remove
    public void destroy() {

    }
}
