/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.common.application.action;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.faces.FacesMessages;

import javax.ejb.Remove;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Name("preferencesModifier")
@Scope(ScopeType.PAGE)
public class PreferencesModifier implements Serializable {

    private static final long serialVersionUID = 6454160821693313591L;

    @In
    private ApplicationPreferenceManager applicationPreferenceManager;

    private Map<String, Object> values;

    @Create
    public void create() {
        values = applicationPreferenceManager.getValues();
    }

    @Remove
    @Destroy
    public void destroy() {

    }

    public Map<String, Object> getValues() {
        return values;
    }

    public String saveValues() {
        List<String> savedKeys = applicationPreferenceManager.persistValues(values);
        if (savedKeys.size() > 0) {
            FacesMessages.instance().add("Preferences saved : " + StringUtils.join(savedKeys, ", "));
        } else {
            FacesMessages.instance().add("No preference changed!");
        }
        ApplicationCacheManager.getApplicationPreferencesCache().clearCache();
        return "/home.seam";
    }

    public void resetHttpHeaders() {
        applicationPreferenceManager.resetHttpHeaders();
        create();
    }
}
