package net.ihe.gazelle.common.report;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.hibernate.internal.SessionImpl;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.Renderer;
import javax.persistence.EntityManager;
import javax.servlet.ServletContext;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ReportRenderer extends Renderer {
    private static Logger log = LoggerFactory.getLogger(ReportRenderer.class);

    /**
     * Returns a JasperReport by its file name. If the report's jasper file is found and its date is before the jrxml file then it is loaded and returned. Otherwise, we look for an jrxml file and
     * compile it to the jasper file first.
     *
     * @param reportName the name of the JasperReport to return.
     * @return the corresponding JasperReport ready to be filled.
     * @throws JRException           if something went wrong.
     * @throws FileNotFoundException
     */
    public static JasperReport getJasperReport(String reportName) throws JRException, FileNotFoundException {
        String jasperFilename = reportName + ".jasper";
        String jrxmlFilename = reportName + ".jrxml";


        File jasperFile = new File(jasperFilename);
        File jrxmlFile = new File(jrxmlFilename);

        if (jasperFile.exists()) {
            if (!jrxmlFile.exists()) {

                return (JasperReport) JRLoader.loadObject(jasperFile);
            } else {
                Date dateJasperFile = new Date(jasperFile.lastModified());
                Date dateJrxmlFile = new Date(jrxmlFile.lastModified());
                if (dateJrxmlFile.before(dateJasperFile)) {
                    return (JasperReport) JRLoader.loadObject(jasperFile);
                }
            }
        }
        if (!jrxmlFile.exists()) {
            throw new FileNotFoundException(" File " + jrxmlFile.getAbsolutePath() + " not found");
        }

        InputStream is = new FileInputStream(jrxmlFile);
        JasperDesign design = JRXmlLoader.load(is);
        JasperReport jasperReport = JasperCompileManager.compileReport(design);
        JRSaver.saveObject(jasperReport, jasperFile);
        return jasperReport;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
        super.encodeEnd(context, component);
        ResponseWriter writer = context.getResponseWriter();
        try {

            ServletContext servlet = (ServletContext) context.getExternalContext().getContext();
            String realPath = servlet.getRealPath("/img/reports");
            String applicationBaseName = ApplicationPreferenceManagerImpl.instance().getApplicationUrlBaseName(
                    (EntityManager) Component.getInstance("entityManager"));

            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            ReportExporterManager.getListOfJRXMLSubReportsAndCompileThem(em);

            ReportComponent reportComponent = (ReportComponent) component;
            String reportName = reportComponent.getReport();
            Map<String, Object> parameters = reportComponent.getParameters();

            String reportDir = ApplicationPreferenceManagerImpl.instance().getGazelleReportsPath();

            int index = reportDir.lastIndexOf(File.pathSeparatorChar);
            String reportPath = reportDir;
            if (index == -1) {
                reportPath += File.separatorChar;
            }
            reportPath += reportName;

            parameters = new HashMap<String, Object>(parameters); // TODO do we
            // really
            // need to
            // copy
            // this?
            parameters.put("SUBREPORT_DIR", reportDir);


            JasperReport jasperReport = getJasperReport(reportPath);

            HashMap<String, Object> hashMap = new HashMap<String, Object>();

            // Get current connection :
            SessionImpl s = (SessionImpl) em.getDelegate();

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, s.connection());
            JRHtmlExporter exporter = new JRHtmlExporter();

            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, writer);
            exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
            exporter.setParameter(JRHtmlExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
            exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "/" + applicationBaseName + "/img/reports/"
                    + reportName);
            exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, hashMap);
            exporter.exportReport();

            writeMapInFile(hashMap, reportName, realPath);
        } catch (JRException | IOException e) {
            log.error("could not generate report", e);
            PrintWriter out = new PrintWriter(writer);
            out.write("<span class=\"bnew\">JasperReports encountered this error :</span>\n");
            out.write("<pre>\n");
            out.write("</pre>\n");
            out.close();
        }
    }

    private void writeMapInFile(HashMap<String, Object> hashMap, String reportName, String realPath) throws IOException {
        if (reportName != null) {
            for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                String currentKey = entry.getKey();
                String newFilePicName = realPath + File.separatorChar + reportName + currentKey;

                File newFilePic = new File(newFilePicName);
                if (!newFilePic.exists()) {
                    if (newFilePic.createNewFile()) {
                        try (FileOutputStream fis = new FileOutputStream(newFilePic)) {
                            fis.write((byte[]) hashMap.get(currentKey));
                        } catch (IOException e) {
                            log.error("Cannot write in file at {}", newFilePicName);
                        }
                    }
                }
            }
        }
    }
}
