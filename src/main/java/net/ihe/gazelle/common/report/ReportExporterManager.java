package net.ihe.gazelle.common.report;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.log.ExceptionLogging;
import net.ihe.gazelle.common.util.DocumentFileUpload;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.*;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;
import net.sf.jasperreports.engine.util.FileResolver;
import net.sf.jasperreports.engine.util.JRXmlUtils;
import net.sf.jasperreports.engine.util.LocalJasperReportsContext;
import net.sf.jasperreports.engine.util.SimpleFileResolver;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.ejb.Remove;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Scope(ScopeType.APPLICATION)
@Name("reportExporterManagerBean")
@GenerateInterface("ReportExporterManagerLocal")
public class ReportExporterManager implements Serializable, ReportExporterManagerLocal {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = 1405721611038200962L;

    private static Logger log = LoggerFactory.getLogger(ReportExporterManager.class);

    /**
     * entityManager is the interface used to interact with the persistence context.
     */
    @In
    private EntityManager entityManager;

    /**
     * Extract the filename from the full path of the name of the file
     *
     * @param reportSource
     * @return the name of the file without the path.
     */
    private static String lastFileName(String reportSource) {
        int pos = reportSource.lastIndexOf(File.separatorChar);
        if (pos == -1) {
            return reportSource;
        } else {
            return reportSource.substring(pos);
        }
    }

    /**
     * @param reportSource
     * @param fileNameDestination
     * @param parameters
     * @throws JRException
     */
    public static void exportToPDF(String reportSource, String fileNameDestination, Map<String, Object> parameters)
            throws JRException {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        JRPdfExporter exporter = new JRPdfExporter();
        String subreports = ApplicationPreferenceManagerImpl.instance().getGazelleReportsPath();
        exportToFormat(reportSource, subreports, fileNameDestination, parameters, em, exporter, "application/pdf");
    }

    public static void exportFromXmlToPDF(String reportSource, final String subdirReportsPath, String xmlContent, String fileNameDestination,
                                          Map<String, Object> parameters) {
        log.info("start to exportFromXmlToPDF");

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        ServletOutputStream servletOutputStream = null;
        try {
            servletOutputStream = response.getOutputStream();
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + fileNameDestination + "\"");

            LocalJasperReportsContext ctx = getLocalJasperReportsContext(subdirReportsPath);
            JasperFillManager jasperFillManager = JasperFillManager.getInstance(ctx);
            JasperExportManager jasperExportManager = JasperExportManager.getInstance(ctx);

            Document document = JRXmlUtils.parse(new InputSource(new StringReader(xmlContent)));

            Map params = initParams(subdirReportsPath, parameters, document);

            generateReportAndExportToPdf(reportSource, subdirReportsPath, params, servletOutputStream, jasperFillManager, jasperExportManager);


        } catch (JRException e) {
            log.error("Failed to parse xml : " + e.getMessage());
        } catch (IOException e) {
            log.error("Failed to get output from response" + e.getMessage());
        } finally {
            try {
                servletOutputStream.flush();
                servletOutputStream.close();
                facesContext.responseComplete();
            } catch (IOException e) {
                log.error("" + e);
            }
        }
    }

    /**
     * This function is using to export a report using Jasper source
     * @param reportSource is the name of the jrxml report source file to produce the report
     * @param subdirReportsPath is the path of the jrxml report directory
     * @param xmlContent is content of the document will be exported
     * @param fileNameDestination is the final name of the MSWord document
     * @param parameters is a list of required parameters to generate the document
     */
    public static void exportFromXmlToMSWord(String reportSource, final String subdirReportsPath, String xmlContent, String fileNameDestination,
                                      Map<String, Object> parameters){

        log.info("start to exportFromXmlToMSWord");

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        ServletOutputStream servletOutputStream = null;

        try {
            servletOutputStream = response.getOutputStream();



            response.setHeader("Content-Disposition", "attachment;filename=report.docx");
            response.setContentType("application/octet-stream");

            LocalJasperReportsContext ctx = getLocalJasperReportsContext(subdirReportsPath);
            JasperFillManager jasperFillManager = JasperFillManager.getInstance(ctx);
            JRDocxExporter jasperExportManager = new JRDocxExporter();

            Document document = JRXmlUtils.parse(new InputSource(new StringReader(xmlContent)));

            Map params = initParams(subdirReportsPath, parameters, document);

            generateReportAndExportToMSWord(reportSource, subdirReportsPath, params, servletOutputStream, jasperFillManager, jasperExportManager);


        } catch (JRException e) {
            log.error("Failed to parse xml : " + e.getMessage());
        } catch (IOException e) {
            log.error("Failed to get output from response" + e.getMessage());
        } finally {
            try {
                servletOutputStream.flush();
                servletOutputStream.close();
                facesContext.responseComplete();
            } catch (IOException e) {
                log.error("" + e);
            }
        }

    }

    private static Map initParams(String subdirReportsPath, Map<String, Object> parameters, Document document) {
        Map params = new HashMap(parameters);
        params.put(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT, document);
        params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, "yyyy-MM-dd");
        params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, "#,##0.##");
        params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.ENGLISH);
        params.put(JRParameter.REPORT_LOCALE, Locale.ENGLISH);
        params.put("SUBREPORT_DIR", subdirReportsPath + File.separatorChar);
        return params;
    }

    /**
     * Generate a document using Jasper files and export it as MSWord Document
     * @param reportSource
     * @param subdirReportsPath
     * @param params
     * @param servletOutputStream
     * @param jasperFillManager
     * @param jasperExportManager
     * @throws JRException
     */
    private static void generateReportAndExportToMSWord(String reportSource, String subdirReportsPath, Map params, ServletOutputStream
            servletOutputStream, JasperFillManager jasperFillManager, JRDocxExporter jasperExportManager) throws JRException {
        ReportExporterManager.getListOfJRXMLSubReportsAndCompileThem(subdirReportsPath);
        log.info("sub report compiled");

        JasperCompileManager.compileReportToFile(reportSource + ".jrxml", reportSource + ".jasper");
        log.info("report compiled");

        JasperFillManager.fillReportToFile(reportSource + ".jasper", params);
        log.info("report filled");

        JasperPrint jasperPrint = jasperFillManager.fill(reportSource + ".jasper", params);
        log.info("jasperPrint created");

        jasperExportManager.setExporterInput(new SimpleExporterInput(jasperPrint));
        jasperExportManager.setExporterOutput(new SimpleOutputStreamExporterOutput(servletOutputStream));
        jasperExportManager.exportReport();
        log.info("report sended");
    }
    private static void generateReportAndExportToPdf(String reportSource, String subdirReportsPath, Map params, ServletOutputStream
            servletOutputStream, JasperFillManager jasperFillManager, JasperExportManager jasperExportManager) throws JRException {
        ReportExporterManager.getListOfJRXMLSubReportsAndCompileThem(subdirReportsPath);
        log.info("sub report compiled");

        JasperCompileManager.compileReportToFile(reportSource + ".jrxml", reportSource + ".jasper");
        log.info("report compiled");

        JasperFillManager.fillReportToFile(reportSource + ".jasper", params);
        log.info("report filled");

        JasperPrint jasperPrint = jasperFillManager.fill(reportSource + ".jasper", params);
        log.info("jasperPrint created");

        jasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
        log.info("report sended");
    }

    private static LocalJasperReportsContext getLocalJasperReportsContext(final String subdirReportsPath) {
        FileResolver resolver = new FileResolver() {
            @Override
            public File resolveFile(String filename) {
                return new File(subdirReportsPath);
            }
        };
        LocalJasperReportsContext ctx = new LocalJasperReportsContext(DefaultJasperReportsContext.getInstance());
        ctx.setClassLoader(ReportExporterManager.class.getClassLoader());
        ctx.setFileResolver(resolver);
        return ctx;
    }

    /**
     * not used, verify if it is used on Product registry or delete it
     *
     * @param reportSource
     * @param fileNameDestination
     * @param parameters
     * @throws JRException
     */
    public static void exportToPDFAndSaveOnServer(String reportSource, String fileName, String fileNameDestination,
                                                  Map<String, Object> parameters, EntityManager em) throws JRException, FileNotFoundException {
        JRPdfExporter exporter = new JRPdfExporter();
        String subreports = ApplicationPreferenceManagerImpl.instance().getGazelleReportsPath();
        parameters.put(JRParameter.REPORT_FILE_RESOLVER, new SimpleFileResolver(new File(subreports)));
        parameters.put("SUBREPORT_DIR", subreports + File.separatorChar);
        FileOutputStream fos = new FileOutputStream(new File(fileNameDestination));
        ReportExporterManager
                .exportToFormatForOutputStream(subreports, reportSource, fileNameDestination, parameters, em,
                        exporter, "application/pdf", fos);
        DocumentFileUpload.showFile(fileNameDestination, fileName, true);

    }

    /**
     * not used on TM, perhaps it is used on Product-registry, otherwise delete it.
     */
    @Deprecated
    public static void exportToUncompressedPDF(String reportSource, String fileNameDestination,
                                               Map<String, Object> parameters) throws JRException {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setParameter(JRPdfExporterParameter.IS_COMPRESSED, false);
        String subreports = ApplicationPreferenceManagerImpl.instance().getGazelleReportsPath();
        exportToFormat(reportSource, subreports, fileNameDestination, parameters, em, exporter, "application/pdf");
    }

    /**
     * not used on TM, perhaps it is used on Product-registry, otherwise delete it.
     */
    @Deprecated
    public static void exportToCSV(String reportSource, String fileNameDestination, Map<String, Object> parameters)
            throws JRException {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        JRCsvExporter exporter = new JRCsvExporter();
        String subreports = ApplicationPreferenceManagerImpl.instance().getGazelleReportsPath();
        exportToFormat(reportSource, subreports, fileNameDestination, parameters, em, exporter, "application/csv");
    }

    /**
     * not used on TM, perhaps it is used on Product-registry, otherwise delete it.
     */
    @Deprecated
    public static void exportToXLS(String reportSource, String fileNameDestination, Map<String, Object> parameters)
            throws JRException {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        JExcelApiExporter exporter = new JExcelApiExporter();
        String subreports = ApplicationPreferenceManagerImpl.instance().getGazelleReportsPath();
        exportToFormat(reportSource, subreports, fileNameDestination, parameters, em, exporter, "application/msexcel");
    }

    /**
     * not used on TM, perhaps it is used on Product-registry, otherwise delete it.
     */
    @Deprecated
    public static void exportToXML(String reportSource, String fileNameDestination, Map<String, Object> parameters)
            throws JRException {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        JRXmlExporter exporter = new JRXmlExporter();
        String subreports = ApplicationPreferenceManagerImpl.instance().getGazelleReportsPath();
        exportToFormat(reportSource, subreports, fileNameDestination, parameters, em, exporter, "text/xml");
    }

    /**
     * not used on TM, perhaps it is used on Product-registry, otherwise delete it.
     */
    @Deprecated
    public static void exportToHTML(String reportSource, String fileNameDestination, Map<String, Object> parameters)
            throws JRException {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        JRHtmlExporter exporter = new JRHtmlExporter();
        String subreports = ApplicationPreferenceManagerImpl.instance().getGazelleReportsPath();
        exportToFormat(reportSource, subreports, fileNameDestination, parameters, em, exporter, "text/html");
    }

    /**
     * @param reportSource        : The source of the report template to use. The full path to the report can be given. If only the name is
     *                            provided and the path is missing. Then the default report location will be
     *                            used.
     * @param fileNameDestination
     * @param parameters
     * @param em                  : The EntityManager
     * @param exporterToUse
     * @param contentType
     * @throws JRException
     */
    private static void exportToFormat(String reportSource, String subdirReports, String fileNameDestination,
                                       Map<String, Object> parameters, EntityManager em, JRAbstractExporter exporterToUse, String contentType)
            throws JRException {
        ServletOutputStream servletOutputStream = null;
        FacesContext context = FacesContext.getCurrentInstance();

        try {
            parameters.put(JRParameter.REPORT_FILE_RESOLVER, new SimpleFileResolver(new File(subdirReports)));
            parameters.put("SUBREPORT_DIR", subdirReports + File.separatorChar);

            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

            response.setContentType(contentType);
            response.setHeader("Content-Disposition", "attachment;filename=\"" + fileNameDestination + "\"");

            servletOutputStream = response.getOutputStream();

            if (exporterToUse instanceof JRHtmlExporter) {
                exporterToUse.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, false);
                exporterToUse.setParameter(JRHtmlExporterParameter.IMAGES_URI, "image?image=");
            }

            ReportExporterManager
                    .exportToFormatForOutputStream(subdirReports, reportSource, fileNameDestination, parameters, em,
                            exporterToUse, contentType, servletOutputStream);

        } catch (IOException e) {
            ExceptionLogging.logException(e, log);
        } finally {
            try {
                servletOutputStream.flush();
                servletOutputStream.close();
                context.responseComplete();
            } catch (IOException e) {
                log.error("" + e);
            }
        }
    }

    public static void exportToFormatForOutputStream(String subdirReports, String reportSource,
                                                     final String fileNameDestination, final Map<String, Object> parameters, EntityManager em,
                                                     final JRAbstractExporter exporterToUse, String contentType, final OutputStream outs)
            throws JRException, FileNotFoundException {

        Session session = (Session) em.getDelegate();

        ReportExporterManager.getListOfJRXMLSubReportsAndCompileThem(em, subdirReports);

        if ((ReportExporterManager.lastFileName(reportSource)).equals(reportSource)) {
            reportSource =
                    ApplicationPreferenceManagerImpl.instance().getGazelleReportsPath() + File.separatorChar + reportSource;
        }
        final JasperReport report = ReportRenderer.getJasperReport(reportSource);

        parameters.put(JRParameter.REPORT_LOCALE, java.util.Locale.ENGLISH);

        session.doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                JasperPrint jasperPrint = null;
                try {
                    jasperPrint = JasperFillManager.fillReport(report, parameters, connection);

                    exporterToUse.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    exporterToUse.setParameter(JRExporterParameter.OUTPUT_STREAM, outs);
                    exporterToUse.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileNameDestination);

                    if (exporterToUse instanceof JRHtmlExporter) {
                        exporterToUse.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, false);
                        exporterToUse.setParameter(JRHtmlExporterParameter.IMAGES_URI, "image?image=");
                    }
                    exporterToUse.exportReport();
                } catch (JRException e) {
                    log.error("" + e.getMessage());
                }
            }
        });

    }

    public static void exportToTxt(String content, String fileNameDestination) {

        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.setContentType("text/plain");

            // if ( fileNameDestination == null ) fileNameDestination = ReportExporterManager.lastFileName(reportSource) ;
            response.setHeader("Content-Disposition", "attachment;filename=" + fileNameDestination);

            ServletOutputStream servletOutputStream;
            servletOutputStream = response.getOutputStream();
            servletOutputStream.write(content.getBytes());
            servletOutputStream.flush();
            servletOutputStream.close();

            context.responseComplete();
        } catch (IOException e) {
            ExceptionLogging.logException(e, log);
        }
    }

    public static void exportToFile(String content, String fileNameDestination) {
        ReportExporterManager.exportToFile(null, content, fileNameDestination);
    }

    public static void exportToFile(String contentType, String content, String fileNameDestination) {
        if (content != null) {
            ReportExporterManager.exportToFile(contentType, content.getBytes(), fileNameDestination);
        }
    }

    public static void exportToFile(String contentType, byte[] bytes, String fileNameDestination) {
        exportToFile(contentType, bytes, fileNameDestination, false);
    }

    public static void exportToFile(String contentType, byte[] bytes, String fileNameDestination, boolean inline) {
        try {
            DocumentFileUpload.showFile(bytes, fileNameDestination, !inline);
        } catch (IOException e) {
            log.warn(e.getMessage());
        }
    }

    /**
     * used by product registry
     */
    @Deprecated
    public static void getListOfJRXMLSubReportsAndCompileThem() {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        getListOfJRXMLSubReportsAndCompileThem(em);
    }

    public static void getListOfJRXMLSubReportsAndCompileThem(EntityManager em) {
        String reportsPath = ApplicationPreferenceManagerImpl.instance().getGazelleReportsPath();
        getListOfJRXMLSubReportsAndCompileThem(em, reportsPath);
    }

    @Deprecated
    public static void getListOfJRXMLSubReportsAndCompileThem(EntityManager em, String reportsPath) {
        getListOfJRXMLSubReportsAndCompileThem(reportsPath);
    }

    public static void getListOfJRXMLSubReportsAndCompileThem(String reportsPath) {
        File f = new File(reportsPath);
        File tmp;
        String[] filesToFilter = f.list();
        File[] files = f.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                String s = file.getName();
                if (s.endsWith(".jrxml")) {
                    try {
                        log.debug(" getJasperReport ->" + f.getAbsolutePath() + File.separatorChar + s.substring(0, s.length() - ".jrxml".length()));
                        ReportRenderer.getJasperReport(f.getAbsolutePath() + File.separatorChar + s.substring(0, s.length() - ".jrxml".length()));
                    } catch (FileNotFoundException e) {
                        ExceptionLogging.logException(e, log);
                    } catch (JRException e) {
                        ExceptionLogging.logException(e, log);
                    }
                }
            } else if (file.isDirectory() && !file.isHidden()) {
                getListOfJRXMLSubReportsAndCompileThem(file.getAbsolutePath());
            }
        }


    }

    @Override
    public String exportToJasperFlash(String reportSource) throws JRException {
        /* ServletContext servlet = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext() ;

		String applicationBaseName = ApplicationPreferenceManager.getApplicationUrlBaseName ( entityManager )  ;

		String reportPath= reportSource;
		Map<String, Object> parameters= new HashMap<String, Object>() ;

		String reportDir=reportPath;
		String reportName = null ;
		int index=reportPath.lastIndexOf('/');
		if(index>0)
		 {
		  reportDir=reportPath.substring(0,index+1);
		  reportName=reportPath.substring(index+1) ;
		 }


		JasperReport jasperReport;
		try
		{
		 jasperReport = ReportRenderer.getJasperReport(reportPath);
		 HashMap<String, Object> hashMap = new HashMap<String, Object>() ;
		 hashMap.put("testingSessionId", new Integer(7)) ;
		 parameters.put("BaseDir", reportDir);

		 //Get current connection :

		 Session s = (Session)entityManager.getDelegate() ;
		 Connection c =   s.connection() ;

		 JasperPrint jasperPrint=JasperFillManager.fillReport(jasperReport, parameters, c  );

		 try{
		    c.close() ;
		    }
		    catch ( Exception e ){
		    }
		//  JRHtmlExporter exporter=new JRHtmlExporter();

		  servlet.setAttribute(BaseHttpServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);

		 return "/administration/listSystemsForAllAIP.xhtml" ;
		}
		catch (FileNotFoundException e)
		{
		FacesMessages.instance().add(e.getMessage()) ;
		return "/home.xhtml" ;
		}
		 */
        return null;

    }

    /**
     * not used on TM, perhaps it is used on Product-registry, otherwise delete it.
     */
    @Override
    @Deprecated
    public void exportToPDF_(String reportSource, String fileNameDestination, Map<String, Object> parameters)
            throws JRException {
        if (parameters == null) {
            parameters = new HashMap<String, Object>();
        }
        ReportExporterManager.exportToPDF(reportSource, fileNameDestination, parameters);
    }

    /**
     * not used on TM, perhaps it is used on Product-registry, otherwise delete it.
     */
    @Override
    @Deprecated
    public void exportToPDF_(String reportSource, String fileNameDestination) throws JRException {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        ReportExporterManager.exportToPDF(reportSource, fileNameDestination, parameters);
    }

    /**
     * Destroy the Manager bean when the session is over.
     */
    @Override
    @Remove
    public void destroy() {

    }

}
