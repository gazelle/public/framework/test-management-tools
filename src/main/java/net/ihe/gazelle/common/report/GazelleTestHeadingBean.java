package net.ihe.gazelle.common.report;

/**
 * @author Anne-Gaelle Berge (aberge@irisa.fr)
 */
public class GazelleTestHeadingBean {


    private String testKeyword = null;
    private String testName = null;
    private Integer pageIndex = null;


    public GazelleTestHeadingBean(String inTestKeyword, String inTestName, Integer inPageIndex) {

        this.testKeyword = inTestKeyword;
        this.testName = inTestName;
        this.pageIndex = inPageIndex;
    }


    public String getTestKeyword() {
        return this.testKeyword;
    }


    public String getTestName() {
        return this.testName;
    }


    public Integer getPageIndex() {
        return this.pageIndex;
    }

}
