package net.ihe.gazelle.common.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.el.ELException;
import javax.faces.FacesException;
import javax.faces.component.UIComponent;
import javax.faces.view.facelets.*;
import java.io.IOException;

public class ParameterTag extends TagHandler {

	private static Logger log = LoggerFactory.getLogger(ParameterTag.class);
	/** Name of the JasperReport parameter. */
	private final TagAttribute name;
	/** Value of the parameter. */
	private final TagAttribute value;
	/** Type to coerce the value to (optional). */
	private final TagAttribute type;

	public ParameterTag(TagConfig config) {
		super(config);
		name = getRequiredAttribute("name");
		value = getRequiredAttribute("value");
		type = getAttribute("type");
	}

	/** Applies the parameters to our parent which should be an instance of ReportComponent. */
	@Override
	public void apply(FaceletContext ctx, UIComponent parent) throws IOException, FacesException, FaceletException,
			ELException {
		if (parent instanceof ReportComponent) {
			ReportComponent reportComponent = (ReportComponent) parent;
			Object o = value.getObject(ctx, getTypeClass(ctx));

			reportComponent.addParameter(name.getValue(ctx), o);
		} else {
			log.warn("parameter tag not inside a report tag!");
		}
		this.nextHandler.apply(ctx, parent);
	}

	/** Returns the type class the value should be cast to. */
	private Class<?> getTypeClass(FaceletContext ctx) {
		try {
			if (type != null) {
				Class<?> clazz = Class.forName(type.getValue(ctx));
				return clazz;
			}
		} catch (ClassNotFoundException e) {
			log.error("cannot convert report parameter " + name.getValue() + " to class " + type.getValue(), e);
		}
		return String.class;
	}

}