package net.ihe.gazelle.common.report;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

/**
 * @author Anne-Gaelle Berge (aberge@irisa.fr)
 */
public class GazelleTestHeadingsScriptlet extends JRDefaultScriptlet {

	private static Logger log = LoggerFactory.getLogger(GazelleTestHeadingsScriptlet.class);


	public Boolean addHeading(String groupName) throws JRScriptletException {

		Collection headings = (Collection) this.getVariableValue("HeadingsCollection");

		String testkeyword = null;
		String testname = null;
		Integer pageIndex = (Integer) this.getVariableValue("PAGE_NUMBER");

		testkeyword = (String) this.getFieldValue("tm_test_keyword");
		testname = (String) this.getFieldValue("tm_test_name");
		headings.add(new GazelleTestHeadingBean(testkeyword, testname, pageIndex));

		return Boolean.FALSE;
	}

	@Override
	public void afterReportInit() throws JRScriptletException {

	}

	@Override
	public void beforePageInit() throws JRScriptletException {

	}

	@Override
	public void afterPageInit() throws JRScriptletException {

	}

	@Override
	public void beforeColumnInit() throws JRScriptletException {

	}

	@Override
	public void afterColumnInit() throws JRScriptletException {

	}

	@Override
	public void beforeGroupInit(String arg0) throws JRScriptletException {

	}

	@Override
	public void afterGroupInit(String arg0) throws JRScriptletException {

	}

	@Override
	public void beforeDetailEval() throws JRScriptletException {

	}

	@Override
	public void afterDetailEval() throws JRScriptletException {
		this.addHeading("test");
	}

}
