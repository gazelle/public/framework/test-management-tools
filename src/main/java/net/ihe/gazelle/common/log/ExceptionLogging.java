package net.ihe.gazelle.common.log;

import org.jboss.seam.security.Identity;
import org.slf4j.Logger;

import java.io.Serializable;

/**
 * <b>Class Description : </b>ExceptionLogging<br>
 * <br>
 * This class manages the exception methods actions. It corresponds to the Business Layer.
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, March 4th
 * @class ExceptionLogging.java
 * @package net.ihe.gazelle.common.util
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 */

public class ExceptionLogging implements Serializable {

    private static final String EXCEPTION_SEPARATOR = "-----------------------------------------------";
    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450973131258793760L;

    public static void logException(Exception e, Logger log) {

        if (log != null) {
            log.error(EXCEPTION_SEPARATOR);
            log.error(EXCEPTION_SEPARATOR);
            log.error("   Logging Exception...(" + e.getClass() + " )");
            log.error(EXCEPTION_SEPARATOR);
            log.error(EXCEPTION_SEPARATOR);


            String usersInfo = Identity.instance().getCredentials().getUsername();
            log.error("User information  = " + usersInfo);

            log.error("Exception Type = " + e.getClass());
            log.error("Exception message = " + e.getMessage());
            log.error("Exception", e);
        }

    }
}
