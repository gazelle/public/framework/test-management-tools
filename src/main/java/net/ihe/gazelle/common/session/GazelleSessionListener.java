/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *    Author : eric.poiseau@inria.fr INRIA
 */
package net.ihe.gazelle.common.session;

import eu.bitwalker.useragentutils.UserAgent;
import net.ihe.gazelle.common.log.ExceptionLogging;
import net.ihe.gazelle.geoip.GeoIP;
import net.ihe.gazelle.geoip.result.Country;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.tm.systems.model.TestingSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.*;

public class GazelleSessionListener implements HttpSessionListener {

    /**
     * When the user session timedout, ( {@link #sessionDestroyed(HttpSessionEvent)})method will be invoked. This method will make necessary cleanups (logging out user, updating db and audit logs,
     * etc...) As a result; after this method, we will be in a clear and stable state. So nothing left to think about because session expired, user can do nothing after this point.
     * <p/>
     * Thanks to hturksoy
     */

    private static Logger log = LoggerFactory.getLogger(GazelleSessionListener.class);

    private static Map<String, HttpSessionUser> sessions = Collections
            .synchronizedMap(new HashMap<String, HttpSessionUser>());

    public GazelleSessionListener() {

    }

    public static HttpSessionUser getSession(String sessionId) {
        return sessions.get(sessionId);
    }

    public static Map<String, HttpSessionUser> getSessions() {
        return sessions;
    }

    public static Map<String, HttpSessionUser> getSortedSessions() {
        Map<String, HttpSessionUser> sortedSessions = sortByUsername(sessions);
        return sortedSessions;
    }

    private static Map<String, HttpSessionUser> sortByUsername(Map<String, HttpSessionUser> inSessions) {
        // Convert Map to List
        List<Map.Entry<String, HttpSessionUser>> list =
                new LinkedList<Map.Entry<String, HttpSessionUser>>(inSessions.entrySet());

        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<String, HttpSessionUser>>() {
            @Override
            public int compare(Map.Entry<String, HttpSessionUser> o1,
                               Map.Entry<String, HttpSessionUser> o2) {
                if (o1.getValue().getUser() == null && o2.getValue().getUser() == null) {
                    return 0;
                }
                if (o1.getValue().getUser() == null) {
                    return 1;
                }
                if (o2.getValue().getUser() == null) {
                    return -1;
                }
                return (o1.getValue().getUser().getLastName()).compareTo(o2.getValue().getUser().getLastName());
            }
        });

        // Convert sorted map back to a Map
        Map<String, HttpSessionUser> sortedMap = new LinkedHashMap<String, HttpSessionUser>();
        for (Iterator<Map.Entry<String, HttpSessionUser>> it = list.iterator(); it.hasNext(); ) {
            Map.Entry<String, HttpSessionUser> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        sessions.put(event.getSession().getId(), new HttpSessionUser(event.getSession()));
        printRuntimeInformations();
    }

    private void printRuntimeInformations() {
        if (log.isInfoEnabled()) {
            Runtime runtime = Runtime.getRuntime();
            long totalMem = runtime.totalMemory() / (1024 * 1024);
            long freeMem = runtime.freeMemory() / (1024 * 1024);
            long usedMem = totalMem - freeMem;
            log.debug("Sessions : " + sessions.keySet().size());
            log.debug("Memory : " + usedMem + " MB / " + totalMem + " MB");
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        // get the destroying session...
        HttpSession session = event.getSession();

        sessions.remove(event.getSession().getId());
        printRuntimeInformations();

        // Only if needed
        try {
            prepareLogoutInfoAndLogoutActiveUser(session);
        } catch (Exception e) {
            ExceptionLogging.logException(e, log);
        }
    }

    /**
     * Clean your logout operations.
     */

    public void prepareLogoutInfoAndLogoutActiveUser(HttpSession httpSession) {
        // Only if needed
    }

    public static class HttpSessionUser {
        private HttpSession session;
        private String countryName = "?";
        private String countryCode = "?";
        private String remoteIP = "?";
        private String browserDetails = "?";

        private String lastAccessedTime = "?";

        private boolean init = false;

        public HttpSessionUser(HttpSession session) {
            super();
            this.session = session;
        }

        public HttpSession getSession() {
            return session;
        }

        public boolean isInit() {
            return init;
        }

        public String getTestingSession() {
            if (getSession() != null) {
                Object testingSessionObject = getSession().getAttribute("selectedTestingSession");
                if ((testingSessionObject != null) && (testingSessionObject instanceof TestingSession)) {
                    return ((TestingSession) testingSessionObject).getDescription();
                }
            }
            return "?";
        }

        public User getUser() {
            if ((session != null) && (session.getAttribute("selectedUser") != null)) {
                User user = (User) session.getAttribute("selectedUser");
                return user;
            }
            return null;
        }

        public String getCountryName() {
            return countryName;
        }

        public String getCountryCode() {
            return countryCode.toLowerCase();
        }

        public String getRemoteIP() {
            return remoteIP;
        }

        public String getBrowserDetails() {
            return browserDetails;
        }

        public void init(HttpServletRequest httpServletRequest) {
            try {
                remoteIP = httpServletRequest.getRemoteAddr();
                String browserInfo = httpServletRequest.getHeader("user-agent");
                lastAccessedTime = new Date().toString();
                try {
                    UserAgent userAgent = UserAgent.parseUserAgentString(browserInfo);
                    browserDetails = userAgent.getOperatingSystem().getName() + " - "
                            + userAgent.getBrowser().getName();
                } catch (Exception e) {
                    browserDetails = browserInfo;
                }

                try {
                    Country country = GeoIP.getCountry_(remoteIP);
                    if ((country != null) && (country.getName() != null)) {
                        countryCode = country.getCode();
                        countryName = country.getName();
                    }
                } catch (Exception e) {
                    countryName = "?";
                }

                init = true;
            } catch (Exception e) {
                log.error("Error", e);
            }
        }

        public String getLastAccessedTime() {
            return lastAccessedTime;
        }

        public void setLastAccessedTime(String lastAccessedTime) {
            this.lastAccessedTime = lastAccessedTime;
        }
    }
}
