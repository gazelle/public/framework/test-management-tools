package net.ihe.gazelle.users.action;

public interface EmailTemplate {

	String getPageName();

}
