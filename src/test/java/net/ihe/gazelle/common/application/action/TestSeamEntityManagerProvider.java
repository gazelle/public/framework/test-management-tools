package net.ihe.gazelle.common.application.action;

import net.ihe.gazelle.hql.providers.detached.AbstractEntityManagerProvider;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import org.kohsuke.MetaInfServices;

@MetaInfServices(net.ihe.gazelle.hql.providers.EntityManagerProvider.class)
public class TestSeamEntityManagerProvider extends AbstractEntityManagerProvider {

    @Override
    public Integer getWeight() {
        return 0;
    }

    @Override
    public String getHibernateConfigPath() {
        return AbstractTestQueryJunit4.HIBERNATE_XML;
    }
}
