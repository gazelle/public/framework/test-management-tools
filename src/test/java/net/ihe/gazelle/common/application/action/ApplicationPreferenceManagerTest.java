package net.ihe.gazelle.common.application.action;

import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.common.model.ApplicationPreference;
import net.ihe.gazelle.common.model.ApplicationPreferenceQuery;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EntityManager.class, EntityManagerService.class, ApplicationPreferenceQuery.class, ApplicationPreferenceManagerImpl.class})
@PowerMockIgnore({"org.xml.*", "org.w3c.*", "javax.xml.*"})
public class ApplicationPreferenceManagerTest extends AbstractTestQueryJunit4 {

    private EntityManager entityManager;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        EntityManagerFactory emf = new HibernatePersistenceProvider().createEntityManagerFactory("PersistenceUnitAppManagerTest", null);
        entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        PowerMockito.mockStatic(EntityManagerService.class);
        when(EntityManagerService.provideEntityManager()).thenReturn(entityManager);

    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
        if (entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().commit();
        }
    }

    @Test
    public void testGetCguLink() {
        Map<String, Object> preferences = new HashMap<>();
        preferences.put("link_to_cgu", "someUrl");

        ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
        applicationPreferenceManager.persistValues(preferences);
        assertEquals("someUrl", applicationPreferenceManager.getCguLink());
    }

    @Test
    public void testRefreshIntervalZero() {
        Map<String, Object> preferences = new HashMap<>();
        ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();

        preferences.put("test_result_refresh_interval", 0);
        applicationPreferenceManager.persistValues(preferences);

        assertEquals(0, applicationPreferenceManager.retrieveTestResultRefreshInterval());
    }

    @Test
    public void testRefreshInterval() {
        Map<String, Object> preferences = new HashMap<>();
        ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();

        preferences.put("test_result_refresh_interval", 10);
        applicationPreferenceManager.persistValues(preferences);

        assertEquals(10000, applicationPreferenceManager.retrieveTestResultRefreshInterval());
    }

    @Test
    public void testCacheProxyChannelStatusFalse() {
        ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
        assertFalse("Uninitialized boolean value, should be false", applicationPreferenceManager.getCASEnabled());
    }


    @Test
    public void testCacheProxyChannelStatus() {
        Map<String, Object> preferences = new HashMap<>();
        preferences.put("cache_proxy_channel_status", true);

        ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
        applicationPreferenceManager.persistValues(preferences);
        assertTrue(applicationPreferenceManager.cacheProxyChannelStatus());
    }

    @Test
    public void test_resetHttpHeaders_create_headers() {
        ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
        applicationPreferenceManager.resetHttpHeaders();

        ApplicationPreferenceQuery apq = new ApplicationPreferenceQuery();
        List<ApplicationPreference> preferences = apq.getListDistinct();
        assertEquals(PreferencesKey.values().length, preferences.size());
    }

    @Test
    public void test_resetHttpHeaders_update_values() {
        ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
        applicationPreferenceManager.resetHttpHeaders();

        ApplicationPreferenceQuery apq = new ApplicationPreferenceQuery();
        List<ApplicationPreference> preferences = apq.getListDistinct();
        assertEquals(PreferencesKey.values().length, preferences.size());

        for (PreferencesKey preferenceKey : PreferencesKey.values()) {
            apq = new ApplicationPreferenceQuery();
            apq.preferenceName().eq(preferenceKey.getFriendlyName());
            ApplicationPreference preference = apq.getUniqueResult();
            preference.setPreferenceValue("testing");
            entityManager.merge(preference);
        }

        for (PreferencesKey preferenceKey : PreferencesKey.values()) {
            apq = new ApplicationPreferenceQuery();
            apq.preferenceName().eq(preferenceKey.getFriendlyName());
            ApplicationPreference preference = apq.getUniqueResult();
            assertNotSame(preference.getPreferenceValue(), preferenceKey.getDefaultValue());
        }

        applicationPreferenceManager.resetHttpHeaders();

        for (PreferencesKey preferenceKey : PreferencesKey.values()) {
            apq = new ApplicationPreferenceQuery();
            apq.preferenceName().eq(preferenceKey.getFriendlyName());
            ApplicationPreference preference = apq.getUniqueResult();
            assertEquals(preference.getPreferenceValue(), preferenceKey.getDefaultValue());
        }
    }


    protected String getHbm2ddl() {
        return "create-drop";
    }
}

